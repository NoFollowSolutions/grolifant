/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.Transformer;
import org.gradle.api.UnknownDomainObjectException;
import org.gradle.api.file.*;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.logging.configuration.ConsoleOutput;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.resources.ReadableResource;
import org.gradle.api.tasks.WorkResult;
import org.gradle.process.ExecResult;
import org.gradle.process.ExecSpec;
import org.gradle.process.JavaExecSpec;
import org.ysb33r.grolifant.internal.core.ProjectOperationsLoader;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Provides various operations which typically would require the {@link org.gradle.api.Project} or
 * {@link org.gradle.api.file.ProjectLayout} instances to resolve.
 *
 * @since 1.0
 */
public interface ProjectOperations {

    static ProjectOperations maybeCreateExtension(Project project) {
        try {
            return find(project);
        } catch (UnknownDomainObjectException e) {
            ProjectOperations po = ProjectOperationsLoader.load(project);
            project.getExtensions().add(ProjectOperations.class, "grolifant", po);
            return po;
        }
    }

    static ProjectOperations create(Project project) {
        try {
            return find(project);
        } catch (UnknownDomainObjectException e) {
            return ProjectOperationsLoader.load(project);
        }
    }

    /**
     * Attempts to find the extension.
     * <p>
     * Do not call this method once the configuration phase has finished.
     *
     * @param project Contextual project
     * @return Extension
     */
    static ProjectOperations find(Project project) {
        return project.getExtensions().getByType(ProjectOperations.class);
    }

    /**
     * Value that can be read safely at configuration time
     *
     * @return Always returns true
     * @since 1.1.0
     */
    default boolean atConfigurationTime() {
        return true;
    }

    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    ReadableResource bzip2Resource(Object file);

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    ReadableResource gzipResource(Object file);

    /**
     * Creates an empty CopySpec.
     *
     * @return {@link CopySpec}
     */
    CopySpec copySpec();

    /**
     * Creates a {@link CopySpec} which can later be used to copy files or create an archive. The given action is used
     * to configure the {@link CopySpec} before it is returned by this method.
     *
     * @param action Action to configure the CopySpec
     * @return The CopySpec
     */
    default CopySpec copySpec(Action<? super CopySpec> action) {
        CopySpec cs = copySpec();
        action.execute(cs);
        return cs;
    }

    /**
     * Safely resolve the stringy item as a path relative to the build directory.
     *
     * @param stringy Any item that can be resolved to a string using
     *                {@code org.ysb33r.grolifant.api.v4.StringUtils.stringize}.
     * @return Provider to a file
     */
    Provider<File> buildDirDescendant(Object stringy);

    /**
     * Performs a copy according to copy specification.
     *
     * @param action Copy specification
     * @return Result of copy operation.
     */
    WorkResult copy(Action<? super CopySpec> action);

    /**
     * Deletes files and directories.
     * <p>
     * This will not follow symlinks. If you need to follow symlinks too use {@link #delete(Action)}.
     *
     * @param paths Any type of object accepted by {@link org.gradle.api.Project#files(Object...)}
     * @return true if anything got deleted, false otherwise
     */
    boolean delete(Object... paths);

    /**
     * Deletes the specified files.  The given action is used to configure a {@link DeleteSpec}, which is then used to
     * delete the files.
     * <p>Example:
     * <pre>
     * project.delete {
     *     delete 'somefile'
     *     followSymlinks = true
     * }
     * </pre>
     *
     * @param action Action to configure the DeleteSpec
     * @return {@link WorkResult} that can be used to check if delete did any work.
     */
    WorkResult delete(Action<? super DeleteSpec> action);

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the an environmental variable.
     * The property cannot safely be read at configuration time.
     * @since 1.1.0
     */
    default Provider<String> environmentVariable(Object name) {
        return environmentVariable(name, false);
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the an environmental variable.
     */
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety);

    /**
     * Executes the specified external process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     */
    ExecResult exec(Action<? super ExecSpec> action);

    /**
     * Provider for the build directory.
     *
     * @return Build directory.
     */
    Provider<File> getBuildDir();

    /**
     * Extensions container for the project.
     *
     * @return Reference to the extensions container
     * @deprecated Will be removed before 1.0 final release
     */
    @Deprecated
    ExtensionContainer getExtensions();

    /**
     * Get the full project path including the root project name in case of a multi-project.
     *
     * @return The fully qualified project path including root project.
     *
     * @since 1.2
     */
    String getFullProjectPath();

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     *
     * @return Directory.
     */
    Provider<File> getGradleUserHomeDir();

    /**
     * Lazy-evaluated project group.
     *
     * @return provider to project group
     */
    Provider<String> getGroupProvider();

    /**
     * Returns an object instance for filesystem operations that deals coprrectly with the functionality of the
     * curretn Gradle version.
     *
     * @return Instance of {@link FileSystemOperations}
     */
    FileSystemOperations getFsOperations();

    /**
     * Returns the project cache directory for the given project.
     *
     * @return Project cache directory. Never {@code null}.
     */
    File getProjectCacheDir();

    /**
     * Returns the project directory.
     *
     * @return Project directory.
     * @since 1.0
     */
    File getProjectDir();

    /**
     * The project name
     *
     * @return Cached value of project name.
     */
    String getProjectName();

    /**
     * Get project path.
     *
     * @return The fully qualified project path.
     *
     * @since 1.2
     */
    String getProjectPath();

    /**
     * A reference to the provider factory.
     *
     * @return {@link ProviderFactory}
     */
    ProviderFactory getProviders();

    /**
     * Tools to deal with provider down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    ProviderTools getProviderTools();

    /**
     * Utilities for working with tasks in a consistent manner across Gradle versions.
     *
     * @return {@link TaskTools} instance.
     *
     * @since 1.3
     */
    TaskTools getTasks();

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    Provider<String> getVersionProvider();

    /**
     * Converts a file-like object to a {@link java.io.File} instance with project context.
     * <p>
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory} (Gradle 4.1+)
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * @param file Potential {@link File} object
     * @return File instance.
     *
     * @deprecated Use {@link FileSystemOperations#file} from {@link #getFsOperations} instead.
     */
    @Deprecated
    File file(Object file);

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     * @since 1.2
     *
     * @deprecated Use {@link FileSystemOperations#fileOrNull} from {@link #getFsOperations} instead.
     */
    @Deprecated
    File fileOrNull(Object file);

    /**
     * Converts a collection of file-like objects to a list of  {@link java.io.File} instances with project context.
     * <p>
     * It will convert anything that the singular version of {@code FileUtils.fileize} can do.
     * In addition it will recursively resolve any collections that result out of resolving the supplied items.
     *
     * @param files List of object to evaluate
     * @return List of resolved files.
     */
    List<File> fileize(Iterable<Object> files);

    /**
     * <p>Creates a {@link FileCollection} containing the given files, as defined by {@link Project#files(Object...)}.
     *
     * <p>This method can also be used to create an empty collection, but the collection may not be mutated later.</p>
     *
     * @param paths The paths to the files. May be empty.
     * @return The file collection. Never returns null.
     */
    FileCollection files(Object... paths);

    /**
     * Creates a new ConfigurableFileTree. The tree will have no base dir specified.
     *
     * @param base Base directory for file tree,
     * @return File tree.
     */
    ConfigurableFileTree fileTree(Object base);

    /**
     * Whether configuration cache is enabled for a build.
     *
     * @return {@code true} is configuration cache is available and enabled.
     */
    boolean isConfigurationCacheEnabled();

    /**
     * Whether Gradle is operating in offline mode.
     *
     * @return {@code true} if offline.
     */
    boolean isOffline();

    /**
     * Whether dependencies should be refreshed.
     *
     * @return {@code true} to check dependencies again.
     */
    boolean isRefreshDependencies();

    /**
     * Whether tasks should be re-ruin
     *
     * @return {@code true} if tasks were set to be re-run.
     */
    boolean isRerunTasks();

    /**
     * Whether current project is the root project.
     *
     * @return {@code true} is current project is root project.
     *
     * @since 1.2
     */
    boolean isRoot();

    /**
     * Executes the specified external java process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     */
    ExecResult javaexec(Action<? super JavaExecSpec> action);

    /**
     * Console output mode
     *
     * @return How the console output has been requested.
     */
    ConsoleOutput getConsoleOutput();

    /**
     * Get the minimum log level for Gradle.
     *
     * @return Log level
     */
    LogLevel getGradleLogLevel();

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the project property.
     * The property cannot safely be read at configuration time
     * @since 1.1.0
     */
    default Provider<String> gradleProperty(Object name) {
        return gradleProperty(name, false);
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the project property.
     */
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety);

    /**
     * Maps one provider to another.
     * Use this method if you need to support Gradle 4.0 - 4.2 as part of compatibility.
     *
     * @param base        Original provider
     * @param transformer Transforming function.
     * @return New provider
     *
     * @deprecated Use {@code getProviderTools().map} instead.
     */
    default <IN, OUT> Provider<OUT> map(Provider<IN> base, Transformer<OUT, IN> transformer) {
        return getProviderTools().map(base, transformer);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name Anything convertible to a string
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     */
    default Provider<String> resolveProperty(Object name) {
        return resolveProperty(name, null, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter.
     * @return Provider to finding a property by the specified name.
     */
    default Provider<String> resolveProperty(Object name, boolean configurationTimeSafety) {
        return resolveProperty(name, null, configurationTimeSafety);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name         Anything convertible to a string
     * @param defaultValue Default value to return if the property search order does not return any value.
     *                     Can be {@code null}. Anything convertible to a string.
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     */
    default Provider<String> resolveProperty(Object name, Object defaultValue) {
        return resolveProperty(name, defaultValue, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name                    Anything convertible to a string
     * @param defaultValue            Default value to return if the property search order does not return any value.
     *                                Can be {@code null}. Anything convertible to a string.
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter.
     * @return Provider to finding a property by the specified name.
     */
    Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety);

    /**
     * Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    <T> Provider<T> provider(Callable<? extends T> var1);

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the system property. The property cannot safely be read at configuration time
     * @since 1.1.0
     */
    default Provider<String> systemProperty(Object name) {
        return systemProperty(name, false);
    }

    /**
     * Creates a provider to a system property.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the system property.
     */
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety);

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     *
     * @deprecated Use {@link FileSystemOperations#relativePath} from {@link #getFsOperations} instead.
     */
    @Deprecated
    String relativePath(Object f);

    /**
     * Returns a TAR tree presentation
     *
     * @param tarPath Path to tar file
     * @return File tree
     */
    FileTree tarTree(Object tarPath);

    /**
     * Updates a file provider.
     * <p>
     * Update property or otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param file     Value that should be lazy-resolved to a file using {@link #file}.
     */
    void updateFileProperty(Provider<File> provider, Object file);

    /**
     * Updates a string provider.
     * <p>
     * Update property or otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param str      Value that should be lazy-resolved to a string .
     */
    void updateStringProperty(Provider<String> provider, Object str);

    /**
     * Returns a ZIP tree presentation
     *
     * @param zipPath Path to zip file
     * @return File tree
     */
    FileTree zipTree(Object zipPath);
}
