/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.provider.Provider;

/**
 * An proxy to provide system properties, project properties and environmental variables as providers.
 *
 * @author Schalk W. Cronje
 * @since 1.1.0
 */
public interface GradleSysEnvProvider {

    Provider<String> systemProperty(Object name, boolean configTime);

    Provider<String> gradleProperty(Object name, boolean configTime);

    Provider<String> environmentVariable(Object name, boolean configTime);

    /**
     * Resolves a property in order of Gradle, system, environment variable.
     * Optionally return a default value if not property was found.
     *
     * @param name         Name of property to resolve. Anything resolvable to a string.
     * @param defaultValue Value to return if property cannot be resolved. Anything resolvable to a string.
     *                     Can be {@code null}.
     * @param configTime   Whether to make the provider safe for usage at configuration time.
     * @return Provider to a property.
     */
    Provider<String> resolve(final Object name, final Object defaultValue, boolean configTime);
}
