/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import groovy.transform.CompileDynamic;
import org.gradle.api.Transformer;
import org.gradle.api.provider.Provider;

/**
 * Tools to safely deal with providers across all Gradle versions 4.0+.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
public interface ProviderTools {
    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @param defaultValue Default value of provider does not have a vlue
     * @return Provider value or default vlue.
     */
    <T> T getOrElse(Provider<T> provider, T defaultValue);

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @return Provider value or {@code null}.
     */
   <T> T getOrNull(Provider<T> provider);

    /**
     * Maps one provider to another.
     * Use this method if you need to support Gradle 4.0 - 4.2 as part of compatibility.
     *
     * @param base        Original provider
     * @param transformer Transforming function.
     * @return New provider
     *
     * @since 1.2
     */
    default <IN, OUT> Provider<OUT> map(Provider<IN> base, Transformer<OUT, IN> transformer) {
        return base.map(transformer);
    }

    /**
     * Allow flatMap functionality for providers even before Gradle 5.0.
     *
     * @param provider Existing provider.
     * @param transformer Transform one provider to another.
     * @param <S> Return type of new provider.
     * @param <T> Return type of existing provider.
     * @return New provider.
     *
     * @since 1.2
     */
    default <S,T> Provider<S> flatMap(
            Provider<T> provider,
            Transformer<? extends Provider<? extends S>,? super T> transformer
    ) {
        return provider.flatMap(transformer);
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     *
     * Returns a Provider whose value is the value of this provider, if present, otherwise the given default value.
     *
     * @param provider Original provider.
     * @param value The default value to use when this provider has no value.
     * @param <T> Provider type.
     * @return Provider value or default value.
     *
     * @since 1.2
     */
    default <T> Provider<T> orElse(Provider<T> provider, T value) {
        return provider.orElse(value);
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * Returns a Provider whose value is the value of this provider, if present, otherwise uses the value from
     * the given provider, if present.
     *
     * @param provider Original provider
     * @param elseProvider The provider whose value should be used when this provider has no value.
     * @param <T> Provider type
     * @return Provider chain.
     *
     * @since 1.2
     */
    default <T> Provider<T> orElse(Provider<T> provider, Provider<? extends T> elseProvider) {
        return provider.orElse(elseProvider);
    }

}
