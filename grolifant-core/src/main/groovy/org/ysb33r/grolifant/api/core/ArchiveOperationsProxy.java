/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.file.FileTree;

/**
 * A proxy for archive operations.
 *
 * This is a compatibility layer for providing operations prior to Gradle 6.6 where this became availble as a service.
 *
 * @since 0.18.0
 */
public interface ArchiveOperationsProxy {
    FileTree tarTree(Object tarPath);

    FileTree zipTree(Object zipPath);
}
