/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.core

import groovy.transform.CompileStatic

@CompileStatic
class ConfigurationCacheWarning {

    static String missingGrolifant50() {
        'The grolifant50 artifact was not on the classpath and you are using a Gradle version ' +
            'where configuration cache is available. It is possible that your build will fail. If so ' +
            'turn off configuration cache in the short term. Upgrade your plugins to use a version that ' +
            'supports configuration caching. You can also add the artifact manually to ' +
            'buildscript.dependencies.classpath'
    }

//    static String missingGrolifant60() {
//        'The grolifant60 artifact was not on the classpath and you are using a Gradle version ' +
//            'where configuration cache is available. It is possible that your build will fail. If so ' +
//            'turn off configuration cache in the short term. Upgrade your plugins to use a version that ' +
//            'supports configuration caching. You can also add the artifact manually to ' +
//            'buildscript.dependencies.classpath'
//    }
}
