/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.core

import groovy.transform.CompileStatic
import org.gradle.StartParameter
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DeleteSpec
import org.gradle.api.file.FileTree
import org.gradle.api.invocation.Gradle
import org.gradle.api.logging.LogLevel
import org.gradle.api.logging.Logger
import org.gradle.api.logging.configuration.ConsoleOutput
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.resources.ReadableResource
import org.gradle.api.resources.ResourceHandler
import org.gradle.api.tasks.WorkResult
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy
import org.ysb33r.grolifant.api.core.ExecOperationsProxy
import org.ysb33r.grolifant.api.core.FileSystemOperations
import org.ysb33r.grolifant.api.core.FileSystemOperationsProxy
import org.ysb33r.grolifant.api.core.GradleSysEnvProvider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.TaskTools
import org.ysb33r.grolifant.internal.core.ConfigurationCache

@SuppressWarnings('MethodCount')
@CompileStatic
abstract class ProjectOperationsProxy implements ProjectOperations {
    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param compressedFile File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource bzip2Resource(Object compressedFile) {
        this.resourceHandler.bzip2(file(compressedFile))
    }

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     * @param compressedFile File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource gzipResource(Object compressedFile) {
        this.resourceHandler.gzip(file(compressedFile))
    }

    /**
     * Performs a copy according to copy specification.
     *
     * @param action Copy specification
     * @return Result of copy operation.
     */
    @Override
    WorkResult copy(Action<? super CopySpec> action) {
        fsOperations.copy(action)
    }

    @Override
    boolean delete(Object... paths) {
        delete { DeleteSpec ds ->
            ds.delete(fileize(paths as List))
        }.didWork
    }

    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        fsOperations.delete(action)
    }

    /**
     * Executes the specified external process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     */
    @Override
    ExecResult exec(Action<? super ExecSpec> action) {
        execOperations.exec(action)
    }

    /** Convert an object to a file
     *
     * @param path Path to convert.
     * @return File object
     */
    @Override
    File file(Object path) {
        fsOperations.file(path)
    }

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     *
     * @since 1.2
     */
    @Override
    File fileOrNull(Object file) {
        fsOperations.fileOrNull(file)
    }

    /**
     * Extensions container for the project.
     *
     * @return Reference to the extensions container
     */
    @Override
    @Deprecated
    ExtensionContainer getExtensions() {
        this.extensions
    }

    /**
     * Returns an object instance for filesystem operations that deals coprrectly with the functionality of the
     * curretn Gradle version.
     *
     * @return Instance of {@link FileSystemOperations}
     */
    @Override
    FileSystemOperations getFsOperations() {
        this.fsOperations
    }

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     *
     * @return Directory.
     */
    @Override
    Provider<File> getGradleUserHomeDir() {
        this.gradleUserHomeDir
    }

    /**
     * A reference to the provider factory.
     *
     * @return {@link ProviderFactory}
     */
    @Override
    ProviderFactory getProviders() {
        this.providerFactory
    }

    /**
     * Utilities for working with tasks in a consistent manner across Gradle versions.
     *
     * @return {@link TaskTools} instance.
     *
     * @since 1.3
     */
    @Override
    TaskTools getTasks() {
        this.taskTools
    }

    /**
     * Whether configuration cache is enabled for a build.
     *
     * @return {@code true} is configuration cache is available and enabled.
     */
    @Override
    boolean isConfigurationCacheEnabled() {
        this.configurationCacheEnabled
    }

    /**
     * Whether Gradle is operating in offline mode.
     *
     * @return {@code true} if offline.
     */
    @Override
    boolean isOffline() {
        this.offline
    }

    /**
     * Whether dependencies should be refreshed.
     *
     * @return {@code true} to check dependencies again.
     */
    @Override
    boolean isRefreshDependencies() {
        this.refreshDependencies
    }

    /**
     * Whether tasks should be re-ruin
     *
     * @return {@code true} if tasks were set to be re-run.
     */
    @Override
    boolean isRerunTasks() {
        this.rerunTasks
    }

    /**
     * Executes the specified external java process.
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     */
    @Override
    ExecResult javaexec(Action<? super JavaExecSpec> action) {
        execOperations.javaexec(action)
    }

    /**
     * Get the minimum log level for Gradle.
     *
     * @return Log level
     */
    @Override
    LogLevel getGradleLogLevel() {
        this.logLevel
    }

    /**
     * Console output mode
     *
     * @return How the console output has been requested.
     */
    @Override
    ConsoleOutput getConsoleOutput() {
        this.consoleOutput
    }

    /**
     * Returns the project directory.
     *
     * @return Project directory.
     */
    @Override
    File getProjectDir() {
        this.projectDir
    }

    /**
     * The project name
     *
     * @return Cached value of project name.
     */
    @Override
    String getProjectName() {
        this.projectName
    }

    /**
     * Get project path.
     *
     * @return The fully qualified project path.
     *
     * @since 1.2
     */
    @Override
    String getProjectPath() {
        this.projectPath
    }

    /**
     * Get the full project path including the root project name in case of a multi-project.
     *
     * @return The fully qualified project path including root project.
     *
     * @since 1.2
     */
    @Override
    String getFullProjectPath() {
        this.fullProjectPath
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the an environmental variable.
     */
    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        propertyProvider.environmentVariable(name, configurationTimeSafety)
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the project property.
     */
    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        propertyProvider.gradleProperty(name, configurationTimeSafety)
    }

    /**
     * Whether current project is the root project.
     *
     * @return {@code true} is current project is root project.
     *
     * @since 1.2
     */
    @Override
    boolean isRoot() {
        this.root
    }

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     *
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePath(Object f) {
        fsOperations.relativePath(f)
    }

    /**
     *
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name Anything convertible to a string
     * @param defaultValue Default value to return if the property search order does not return any value.
     *                                Can be {@code null}. Anything convertible to a string.
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter.
     * @return Provider to finding a property by the specified name.
     */
    @Override
    Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety) {
        propertyProvider.resolve(name, defaultValue, configurationTimeSafety)
    }

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter
     * @return Provider to the value of the system property.
     */
    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        propertyProvider.resolve(name, null, configurationTimeSafety)
    }

    /**
     * Returns a TAR tree presentation
     *
     * @param tarPath Path to tar file
     * @return File tree
     */
    @Override
    FileTree tarTree(Object tarPath) {
        archiveOperations.tarTree(tarPath)
    }

    /**
     * Returns a ZIP tree presentation
     *
     * @param zipPath Path to zip file
     * @return File tree
     */
    @Override
    FileTree zipTree(Object zipPath) {
        archiveOperations.zipTree(zipPath)
    }

    @Deprecated
    @SuppressWarnings('LineLength')
    protected FileSystemOperationsProxy getFileSystemOperations() {
        projectLogger.warn("Usage of legacy ${this.class.canonicalName}#getFileSystemOperations found. " +
            'Please raise an issue against the appropriate Gradle plugin as it is most probably using a very old version of Grolifant.',
            new RuntimeException())
        this.legacyFsOperationsProxy
    }

    abstract protected ArchiveOperationsProxy getArchiveOperations()

    abstract protected ExecOperationsProxy getExecOperations()

    abstract protected GradleSysEnvProvider getPropertyProvider()

    protected ProjectOperationsProxy(
        Project tempProjectReference,
        FileSystemOperations fsOperations,
        TaskTools taskTools1
    ) {
        this.extensions = tempProjectReference.extensions
        this.providerFactory = tempProjectReference.providers
        this.resourceHandler = tempProjectReference.resources
        this.fsOperations = fsOperations
        this.taskTools = taskTools1

        Gradle gradle = tempProjectReference.gradle
        StartParameter startParameter = gradle.startParameter

        this.root = tempProjectReference == tempProjectReference.rootProject
        this.offline = startParameter.offline
        this.logLevel = startParameter.logLevel
        this.refreshDependencies = startParameter.refreshDependencies
        this.rerunTasks = startParameter.rerunTasks
        this.gradleUserHomeDir = providerFactory.provider({ File f -> f }.curry(gradle.gradleUserHomeDir))
        this.consoleOutput = startParameter.consoleOutput
        this.projectDir = tempProjectReference.projectDir
        this.configurationCacheEnabled = ConfigurationCache.isEnabled(tempProjectReference)
        this.projectName = tempProjectReference.name
        this.projectPath = tempProjectReference.path
        this.fullProjectPath = root ? this.projectName : "${tempProjectReference.rootProject.name}${projectPath}"

        this.legacyFsOperationsProxy = new LegacyFileSystemOperationsProxy(proxy: this.fsOperations)
        this.projectLogger = tempProjectReference.logger
    }

    @Deprecated
    private final ExtensionContainer extensions // TODO: Remove
    private final ProviderFactory providerFactory
    private final Provider<File> gradleUserHomeDir
    private final File projectDir
    private final String projectName
    private final String projectPath
    private final String fullProjectPath
    private final boolean root
    private final boolean offline
    private final boolean refreshDependencies
    private final boolean rerunTasks
    private final LogLevel logLevel
    private final ConsoleOutput consoleOutput
    private final ResourceHandler resourceHandler
    private final boolean configurationCacheEnabled
    private final FileSystemOperations fsOperations
    private final TaskTools taskTools

    @Deprecated
    private final Logger projectLogger

    @Deprecated
    private final FileSystemOperationsProxy legacyFsOperationsProxy

    @Deprecated
    private static class LegacyFileSystemOperationsProxy implements FileSystemOperationsProxy {
        @Delegate
        FileSystemOperations proxy
    }
}
