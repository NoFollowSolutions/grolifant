/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.core

import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.FileSystemOperations

import java.nio.file.Path

/**
 * Common filesystem operations
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.3
 */
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class FileSystemOperationsProxy implements FileSystemOperations {

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context.
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePath(Object f) {
        relativizeImpl(projectDirPath, file(f).toPath())
    }

    /**
     * Returns the relative path from the root project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     *
     */
    @Override
    String relativeRootPath(Object f) {
        relativizeImpl(rootDirPath, file(f).toPath())
    }

    /**
     * Returns the relative path from the given path to the project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePathToProjectDir(Object f) {
        relativizeImpl(file(f).toPath(), projectDirPath)
    }

    /**
     * Returns the relative path from the given path to the root project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePathToRootDir(Object f) {
        relativizeImpl(file(f).toPath(), rootDirPath)
    }

    /**
     * The project directory.
     */
    protected final File projectDir

    /**
     * The root project directory.
     */
    protected final File rootDir

    /**
     * The project directory as {@link Path}.
     */
    protected final Path projectDirPath

    /**
     * The project root directory as {@link Path}.
     */
    protected final Path rootDirPath

    protected FileSystemOperationsProxy(Project tempProjectReference) {
        this.projectDir = tempProjectReference.projectDir
        this.rootDir = tempProjectReference.rootDir
        this.projectDirPath = tempProjectReference.projectDir.toPath()
        this.rootDirPath = tempProjectReference.rootDir.toPath()
    }

    private String relativizeImpl(Path base, Path target) {
        base.relativize(target).toFile().toString()
    }
}