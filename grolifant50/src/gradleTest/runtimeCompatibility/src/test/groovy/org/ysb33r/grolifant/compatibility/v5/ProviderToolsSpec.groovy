/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v5

import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import spock.lang.Specification
import sun.nio.cs.US_ASCII

class ProviderToolsSpec extends Specification {

    public static final String INITIAL_STRING = 'foo'
    public static final String FINAL_STRING = 'bar'

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations = ProjectOperations.maybeCreateExtension(project)

    void 'Can map a provider'() {
        setup:
        def provider = project.provider { -> INITIAL_STRING }

        when:
        def newProvider = projectOperations.providerTools.map provider, { FINAL_STRING }

        then:
        newProvider.get() == FINAL_STRING
    }

    void 'Can flatMap a provider'() {
        setup:
        Provider<String> provider = project.provider { -> INITIAL_STRING }

        when:
        def newProvider = projectOperations.providerTools.flatMap(provider) { String fromProvider ->
            (project.provider { -> fromProvider.size() }) as Provider<Integer>
        }

        then:
        newProvider.get() == INITIAL_STRING.size()
    }

    void 'Can orElse a provider with a fixed value'() {
        setup:
        Provider<String> provider = project.provider { -> null }

        when:
        def newProvider = projectOperations.providerTools.orElse(provider, FINAL_STRING)

        then:
        newProvider.get() == FINAL_STRING
    }

    void 'Can orElse a provider with another provider'() {
        setup:
        Provider<String> provider = project.provider { -> null }

        when:
        def newProvider = projectOperations.providerTools.orElse(
            provider,
            project.provider { -> INITIAL_STRING }
        )

        then:
        newProvider.get() == INITIAL_STRING
    }
}