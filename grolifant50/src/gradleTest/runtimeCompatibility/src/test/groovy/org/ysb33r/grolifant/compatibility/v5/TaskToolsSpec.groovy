/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v5

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Copy
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import spock.lang.Specification

class TaskToolsSpec extends Specification {
    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    void setup() {
        projectOperations = ProjectOperations.maybeCreateExtension(project)
    }

    void 'Register a task with TaskTools'() {
        when:
        projectOperations.tasks.register('foo', Copy) {
            it.into 'foo'
        }
        projectOperations.tasks.named('foo', Copy) {
            it.from 'bar'
        }

        then:
        project.tasks.getByName('foo')
    }

    void 'Register intent to configure a task'() {
        setup:
        boolean configurationCompleted = false
        projectOperations.tasks.whenNamed('foo') {
            configurationCompleted = true
        }

        when:
        project.tasks.register('foo')

        then:
        !configurationCompleted

        when:
        project.tasks.getByName('foo')

        then:
        configurationCompleted
    }

    void 'Register intent to configure a task of a specific type'() {
        setup:
        boolean configurationCompleted = false
        projectOperations.tasks.whenNamed('foo', Copy) {
            configurationCompleted = true
        }
        // tag::whenNamed[]
        projectOperations.tasks.whenNamed('foo', Copy) {
            it.from 'bar'
        }
        // end::whenNamed[]

        when:
        project.tasks.register('foo', Copy)

        then:
        !configurationCompleted

        when:
        project.tasks.getByName('foo')

        then:
        configurationCompleted
    }

    void 'Create a task provider from a string'() {
        setup:
        def myTask = project.tasks.register('foo')

        when:
        // tag::taskize[]
        projectOperations.tasks.named('foo') { Task t ->
            t.dependsOn(projectOperations.tasks.taskize('nameOfTask1')) // <1>
            t.dependsOn(projectOperations.tasks.taskize({ -> 'nameOfTask2' })) // <2>
            t.dependsOn(projectOperations.tasks.taskize(['nameOfTask3', 'nameofTask4'])) // <3>
        }
        // end::taskize[]

        then:
        project.tasks.getByName('foo').dependsOn.size() == 3
    }
}