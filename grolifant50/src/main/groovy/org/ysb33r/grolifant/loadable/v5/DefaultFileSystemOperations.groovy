/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v5

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DeleteSpec
import org.gradle.api.file.ProjectLayout
import org.gradle.api.tasks.WorkResult
import org.ysb33r.grolifant.api.core.FileSystemOperations
import org.ysb33r.grolifant.loadable.core.FileSystemOperationsProxy

import static org.ysb33r.grolifant.api.v5.FileUtils.fileize
import static org.ysb33r.grolifant.api.v5.FileUtils.fileizeOrNull

/**
 *
 * Implements {@link FileSystemOperations} for Gradle 5.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.3
 */
@CompileStatic
class DefaultFileSystemOperations extends FileSystemOperationsProxy {

    DefaultFileSystemOperations(Project project) {
        super(project)
        this.project = project
        this.projectLayout = project.layout
    }

    /**
     * Copies the specified files.
     * @param action Configures a {@link CopySpec}
     * @return Result of copy to check whether it was successful.
     */
    @Override
    WorkResult copy(Action<? super CopySpec> action) {
        project.copy(action)
    }

    /**
     * Deletes the specified files.
     * @param action Configures a {@link DeleteSpec}
     * @return Result of deletion to check whether it was successful.
     */
    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        project.delete(action)
    }

    /**
     * Converts a file-like object to a {@link java.io.File} instance with project context.
     * <p>
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory} (Gradle 4.1+)
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * @param file Potential {@link File} object
     * @return File instance.
     */
    @Override
    File file(Object file) {
        fileize(projectLayout, file)
    }

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     */
    @Override
    File fileOrNull(Object file) {
        fileizeOrNull(projectLayout, file)
    }

    /**
     * Synchronizes the contents of a destination directory with some source directories and files.
     *
     * @param action Action to configure the CopySpec.
     * @return {@link WorkResult} that can be used to check if the sync did any work.
     */
    @Override
    WorkResult sync(Action<? super CopySpec> action) {
        project.sync(action)
    }

    private final Project project
    private final ProjectLayout projectLayout
}
