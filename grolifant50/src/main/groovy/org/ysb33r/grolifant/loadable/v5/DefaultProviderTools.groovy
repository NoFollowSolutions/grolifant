/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v5

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.ProviderTools

/**
 * Safely deal with Providers down to Gradle 5.0.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
class DefaultProviderTools implements ProviderTools {

    DefaultProviderTools(ProviderFactory providerFactory) {
        this.providerFactory = providerFactory
    }

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @param defaultValue Default value of provider does not have a vlue
     * @return Provider value or default vlue.
     */
    @Override
    public <T> T getOrElse(Provider<T> provider, T defaultValue) {
        provider.getOrElse(defaultValue)
    }

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @return Provider value or {@code null}.
     */
    @Override
    public <T> T getOrNull(Provider<T> provider) {
        provider.getOrNull()
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     *
     * Returns a Provider whose value is the value of this provider, if present, otherwise the given default value.
     *
     * @param provider Original provider.
     * @param value The default value to use when this provider has no value.
     * @param <T >   Provider type.
     * @return Provider value or default value.
     *
     * @since 1.2
     */
    @Override
    @CompileDynamic
    public <T> Provider<T> orElse(Provider<T> provider, T value) {
        if (LegacyLevel.PRE_5_6) {
            providerFactory.provider { ->
                getOrElse(provider, value)
            }
        } else {
            provider.orElse(value)
        }
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * Returns a Provider whose value is the value of this provider, if present, otherwise uses the value from the
     * given provider, if present.
     *
     * @param provider Original provider
     * @param elseProvider The provider whose value should be used when this provider has no value.
     * @param <T >   Provider type
     * @return Provider chain.
     *
     * @since 1.2
     */
    @Override
    @CompileDynamic
    public <T> Provider<T> orElse(Provider<T> provider, Provider<? extends T> elseProvider) {
        if (LegacyLevel.PRE_5_6) {
            providerFactory.provider { ->
                provider.present ? provider.get() : elseProvider.get()
            }
        } else {
            provider.orElse(elseProvider)
        }
    }

    private final ProviderFactory providerFactory
}
