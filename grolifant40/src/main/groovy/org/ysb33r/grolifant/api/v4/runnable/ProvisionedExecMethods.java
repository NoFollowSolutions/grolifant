/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.Action;
import org.gradle.process.ExecResult;
import org.ysb33r.grolifant.api.core.ProjectOperations;

public interface ProvisionedExecMethods<E extends AbstractExecSpec<E>> extends ExecMethods<E> {

    /**
     * Grolifant's configuration cache-safe operations.
     *
     * @return instance.
     */
    ProjectOperations getProjectOperations();

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     */
    E createExecSpec();

    @Override
    default ExecResult exec(E spec) {
        return getProjectOperations().exec(execSpec -> spec.copyToExecSpec(execSpec));
    }

    @Override
    default ExecResult exec(Action<E> specConfigurator) {
        E spec = createExecSpec();
        specConfigurator.execute(spec);
        return exec(spec);
    }
}
