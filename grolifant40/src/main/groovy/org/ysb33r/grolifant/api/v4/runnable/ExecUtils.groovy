/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant.api.core.ProjectOperations

import java.util.function.Function

/**
 * Utilities to help with building runnable classes.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
class ExecUtils {

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param projectOperations ProjectOperations instance.
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser A parser for the output of running the executable which could extract the version.
     * @param configurator Optional configurator to customise the execution specification.
     * @return The version string.
     */
    static String parseVersionFromOutput(
        ProjectOperations projectOperations,
        Iterable<String> argsForVersion,
        File executablePath,
        Function<String, String> versionParser,
        Action<ExecSpec> configurator = null
    ) {
        def strm = new ByteArrayOutputStream()

        Action<ExecSpec> runner = new Action<ExecSpec>() {
            @Override
            void execute(ExecSpec spec) {
                spec.standardOutput = strm
                spec.executable(executablePath)
                spec.args = argsForVersion

                if (configurator != null) {
                    configurator.execute(spec)
                }
            }
        }

        projectOperations.exec(runner).assertNormalExitValue()
        versionParser.apply(strm.toString())
    }
}
