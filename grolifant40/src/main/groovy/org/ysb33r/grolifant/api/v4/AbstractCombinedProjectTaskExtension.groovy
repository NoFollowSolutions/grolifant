/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.logging.Logger
import org.gradle.api.plugins.ExtensionContainer
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.ProjectOperations

/** Base class for an extension that can both be used on a project or a task.
 *
 * @since 0.4
 *
 * @deprecated Use {@link CombinedProjectTaskExtensionBase} instead.
 */
@CompileStatic
@Deprecated
class AbstractCombinedProjectTaskExtension {

    /** Uses reflection to invoke a method on a project extension or a task extension.
     * If the extension is attached to a task and the returned value is null, it will first try to resolve the value
     * by the project extension.
     *
     * @param methodName Method to invoke.
     * @return Object or {@code null}.
     */
    Object getValueByMethod(final String methodName) {
        Object ret = resolveValueByMethod(this, methodName)
        if (ret == null && task != null) {
            ret = resolveValueByMethod(projectExtension, methodName)
        }
        ret
    }

    /** Uses a closure to resolve a value on a project extension or a task extension.
     * If the extension is attached to a task and the returned value is null, it will first try to resolve the value
     * by the project extension.
     *
     * @param getter A closure which will be passed an extension object which is an implementation of
     * {@code AbstractCombinedProjectTaskExtension}.
     * @return Object or {@code null}.
     */
    Object getValue(Closure getter) {
        Object result = getter(this)
        if (result == null && task != null) {
            result = getter(projectExtension)
        }
        result
    }

    /** Attach this extension to a project
     *
     * @param project Project to attach to.
     */
    protected AbstractCombinedProjectTaskExtension(Project project) {
        this.project = project // TODO: This will be removed in a future release
        this.projectExtensions = project.extensions // TODO: Remove, unsafe design.
        this.projectOperations = ProjectOperations.create(project)
        this.logger = project.logger
        this.projectExtension = this
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to
     */
    protected AbstractCombinedProjectTaskExtension(Task task, final String projectExtName) {
        this.task = task
        this.projectExtName = projectExtName
        this.projectExtensions = task.project.extensions  // TODO: Remove, unsafe design.
        this.projectOperations = ProjectOperations.create(task.project)
        this.logger = task.logger
        this.projectExtension = (AbstractCombinedProjectTaskExtension) (
            task.project.extensions.getByName(projectExtName)
        )
    }

    /** Project this extension is associated with.
     *
     * @return {@code task ? task.project : project}
     * @deprecated Use{@link #getProjectOperations}, {@link #getProjectExtensions}
     */
    @Deprecated
    protected Project getProject() {
        if (!LegacyLevel.PRE_6_6) {
            logger.warn('You are trying to access the project instance. ' +
                'On Gradle 6.6+. This might no longer be safe to do, due to configuration cache. ' +
                'Use getProjectOperations() instead.'
            )
        }
        task?.project ?: project
    }

    /**
     * Returns the project's extension container, but safe in configuration cache context.
     *
     * @return Equivalent of {@code project.extensions}
     *
     * @since 1.0*
     */
    @Deprecated
    protected ExtensionContainer getProjectExtensions() {
        this.projectExtensions
    }

    /** Returns a instance of {@link ProjectOperations}.
     *
     * @return {@link ProjectOperations}
     *
     * @since 1.0
     */
    protected ProjectOperations getProjectOperations() {
        this.projectOperations
    }

    /** Task this extension is attached to.
     *
     * @return Task or {@code null} if extension is not attached to a task.
     */
    protected Task getTask() {
        this.task
    }

    /** Returns the extension that is attached to the project.
     *
     * This method should not be called in Gradle's execution phase.
     *
     * @return Extension as attached to the project
     * @throw {@link org.gradle.api.UnknownDomainObjectException} is extension does not exist.
     */
    protected AbstractCombinedProjectTaskExtension getProjectExtension() {
        this.projectExtension
    }

    @CompileDynamic
    private Object resolveValueByMethod(AbstractCombinedProjectTaskExtension ext, final String methodName) {
        ext."${methodName}"()
    }

    private final ProjectOperations projectOperations
    private final ExtensionContainer projectExtensions // TODO: Remove, unsafe design
    private final Logger logger
    private final Task task
    private final String projectExtName
    private final AbstractCombinedProjectTaskExtension projectExtension
    private final Project project // TODO: Remove in a future version.
}
