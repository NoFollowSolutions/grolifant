/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.DefaultTask;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.TaskAction;
import org.gradle.process.ExecResult;
import org.ysb33r.grolifant.api.core.ProjectOperations;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.ysb33r.grolifant.api.v4.MapUtils.stringizeValues;

/**
 * Base task class to wrap external tool executions without exposing command-line parameters directly.
 *
 * <p> {@code E} is the execution specification.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public abstract class AbstractExecWrapperTask<E extends ExecutableExecSpec<E>> extends DefaultTask {

    /**
     * Replace current environment with new one.
     *
     * @param args New environment key-value map of properties.
     */
    public void setEnvironment(Map<String, ?> args) {
        this.env.clear();
        this.env.putAll(args);
    }

    /**
     * Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     */
    @Input
    public Provider<Map<String, String>> getEnvironmentProvider() {
        return this.envProvider;
    }

    /**
     * Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     */
    public void environment(Map<String, ?> args) {
        this.env.putAll(args);
    }

    /**
     * The default implementation will build an execution specification
     * and run it.
     */
    @TaskAction
    protected void exec() {
        E execSpec = createExecSpec();
        addExecutableToExecSpec(execSpec);
        addEnvironmentToExecSpec(execSpec);
        configureExecSpec(execSpec);
        runExecSpec(execSpec);
    }

    protected AbstractExecWrapperTask() {
        super();
        this.projectOperations = ProjectOperations.find(getProject());
        this.envProvider = getProject().provider(() -> stringizeValues(env));
    }

    /**
     * Creates a new execution specification.
     *
     * @return New exec specification
     */
    abstract protected E createExecSpec();

    /**
     * Location of executable
     *
     * @return Location as {@link File}.
     */
    @Internal
    abstract protected File getExecutableLocation();

    /**
     * Configures an execution specification from task properties
     *
     * @param execSpec Execution specification to modify.
     */
    abstract protected void configureExecSpec(E execSpec);

    /**
     * Project operations that are configuration cache-safe.
     *
     * @return Instance
     */
    @Internal
    protected ProjectOperations getProjectOperations() {
        return this.projectOperations;
    }

    /**
     * Adds the exe to the execution specification.
     *
     * @param execSpec Execution specification to modify.
     */
    protected void addExecutableToExecSpec(E execSpec) {
        execSpec.executable(getExecutableLocation().getAbsolutePath());
    }

    /**
     * Adds the configured environment to the execution specification.
     *
     * @param execSpec Execution specification to modify.
     */
    protected void addEnvironmentToExecSpec(E execSpec) {
        execSpec.environment(getEnvironmentProvider().get());
    }

    /**
     * Executes a specification.
     *
     * This simply runs the executable with the specified configuration.
     *
     * For precise control override this method.
     *
     * @param execSpec Execution specification to be executed
     * @return
     */
    protected ExecResult runExecSpec(E execSpec) {
        return projectOperations.exec(target -> execSpec.copyTo(target));
    }

    private final ProjectOperations projectOperations;
    private final Map<String, Object> env = new LinkedHashMap<String, Object>();
    private final Provider<Map<String, String>> envProvider;
}
