/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.downloader;

import java.io.File;
import java.util.function.Function;

/**
 * Verifies a artifact root
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ArtifactRootVerification extends Function<File, File> {
    /**
     * Verifies a artifact root
     *
     * @param unpackedRoot Directory where unpacked package is unpacked
     *                     (or in case of a single file the parent directory).
     * @return The correctly verified root. (Could be a child of the unpacked root).
     * @throw Throws{@link DistributionFailedException} if verification failed
     */
    default File verify(File unpackedRoot) {
        return apply(unpackedRoot);
    }
}
