/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.internal.v4.ProviderHelpers
import org.ysb33r.grolifant.internal.v4.property.order.StandardPropertyResolveOrders

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_6

/** Resolves properties in a certain i.e. like SpringBoot, but
 * less functionality to suite Gradle context.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.15.0
 */
@CompileStatic
class PropertyResolver {

    public static final PropertyResolveOrder PROJECT_SYSTEM_ENV =
        new StandardPropertyResolveOrders.ProjectSystemEnvironment()
    public static final PropertyResolveOrder SYSTEM_ENV_PROPERTY =
        new StandardPropertyResolveOrders.SystemEnvironmentProject()

    /** Creates a property resolver that will use {@link #PROJECT_SYSTEM_ENV} by default.
     *
     * @param project Project context in which to resolve properties.
     * The project will not be cached, but is used to obtain a reference to the project's properties.
     */
    PropertyResolver(Project project) {
        this.order = PROJECT_SYSTEM_ENV
        this.projectOperations = ProjectOperations.find(project)
    }

    /** Creates a property resolver with a custom resolve order
     *
     * @param project Project context in which to resolve properties.
     * @param order Custom property resolve order
     */
    PropertyResolver(Project project, PropertyResolveOrder order) {
        this.order = order
        this.projectOperations = ProjectOperations.find(project)
    }

    /** Creates a property resolver with a custom resolve order
     *
     * @param projectOperations Project context in which to resolve properties.
     * @param order Custom property resolve order
     */
    PropertyResolver(ProjectOperations projectOperations) {
        this.order = PROJECT_SYSTEM_ENV
        this.projectOperations = projectOperations
    }

    /** Change the existing property order
     *
     * @param newOrder New property resolve order.
     */
    void order(PropertyResolveOrder newOrder) {
        this.order = newOrder
    }

    /** Gets a property.
     *
     * @param name Name of property to resolve
     * @return Resolved property or {@code null} if no property was found.
     */
    String get(final String name) {
        get(name, null, this.order)
    }

    /** Gets a provider to a property.
     *
     * @param name Name of property to resolve.
     * @return Provider to a property.
     *
     * @since 1.1
     */
    Provider<String> provide(final String name) {
        provide(name, null, this.order, false)
    }

    /** Gets a provider to a property that is safe to use at configuration-time.
     *
     * @param name Name of property to resolve.
     * @return Provider to a property.
     *
     * @since 1.1
     */
    Provider<String> provideAtConfiguration(final String name) {
        provide(name, null, this.order, true)
    }

    /** Gets a property.
     *
     * @param name Name of property to resolve.
     * @param defaultValue Value to return if property cannot be resolved.
     * @return Resolved property or {@code defaultValue} if no property was found.
     */
    String get(final String name, final String defaultValue) {
        get(name, defaultValue, this.order)
    }

    /** Gets a provider to a property.
     *
     * @param name Name of property to resolve.
     * @param defaultValue Value to return if property cannot be resolved.
     * @return Provider to a property.
     *
     * @since 1.1
     */
    Provider<String> provide(final String name, final String defaultValue) {
        provide(name, defaultValue, this.order, false)
    }

    /** Gets a provider to a property that is safe to use at configuration-time.
     *
     * @param name Name of property to resolve.
     * @param defaultValue Value to return if property cannot be resolved.
     * @return Provider to a property.
     *
     * @since 1.1
     */
    Provider<String> provideAtConfiguration(final String name, final String defaultValue) {
        provide(name, defaultValue, this.order, true)
    }

    /** Gets a property using a specific resolve order.
     *
     * @param name Name of property to resolve
     * @param order Resolve order
     * @return Resolved property or {@code null} if no property was found.
     */
    String get(final String name, PropertyResolveOrder order) {
        get(name, null, order)
    }

    /** Gets a property using a specific resolve order
     *
     * @param name Name of property to resolve.
     * @param defaultValue Value to return if property cannot be resolved.
     * @param order Resolve order.
     * @return Resolved property or {@code defaultValue} if no property was found.
     */
    String get(final String name, final String defaultValue, PropertyResolveOrder order) {
        ProviderHelpers.getOrNull(provide(name, defaultValue, order, true))
    }

    /**
     * Gets a provider to a property using a specific resolve order.
     *
     * @param name Name of property to resolve.
     * @param defaultValue Value to resturn if property cannot be resolved. (Can be @code null).
     * @param order Resolve order.
     * @param atConfigurationTime Whether to make the provider safe for usage at configuration time.
     * @return Provider to a property.
     *
     * @since 1.1
     */
    Provider<String> provide(
        final String name,
        final String defaultValue,
        PropertyResolveOrder order,
        boolean atConfigurationTime
    ) {
        if (defaultValue != null) {
            valueOrElse(order.resolve(projectOperations, name, atConfigurationTime), defaultValue)
        } else {
            order.resolve(projectOperations, name, atConfigurationTime)
        }
    }

    @CompileDynamic
    private Provider<String> valueOrElse(Provider<String> provider, final String defaultValue) {
        if (PRE_5_6) {
            projectOperations.provider { ->
                ProviderHelpers.getOrElse(provider, defaultValue)
            }
        } else {
            provider.orElse(projectOperations.provider { -> defaultValue })
        }
    }
    private final ProjectOperations projectOperations
    private PropertyResolveOrder order
}
