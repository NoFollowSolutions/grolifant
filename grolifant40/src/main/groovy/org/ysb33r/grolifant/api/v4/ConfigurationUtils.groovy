/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Provider

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_5

/** Utilities to deal with {@link org.gradle.api.artifacts.Configuration} instances.
 *
 * @since 1.0.0
 */
@CompileStatic
class ConfigurationUtils {

    /** Tries to resolve a configuration from the provided object.
     *
     * @param project Project context
     * @param configurationThingy A configuration instance of something convertible to a string.
     * @return Configuration entity
     *
     */
    static Configuration asConfiguration(Project project, Object configurationThingy) {
        if (configurationThingy instanceof Configuration) {
            (Configuration) configurationThingy
        } else {
            project.configurations.getByName(stringize(configurationThingy))
        }
    }

    /** Converts a collection of items to configurations. Closures are evaluated as well.
     *
     * @param configurationThings Iterable list of objects that can be converted directly to
     * {@link org.gradle.api.artifacts.Configuration} instances or indirectly by being convertible to strings.
     *   Returned collections, providers, closures etc. will be recursively evaluated.
     * @return A list of Configuration instances.
     */
    static List<Configuration> asConfigurations(Project project, final Iterable<?> configurationThings) {
        List<Configuration> collection = []

        for (Object item in configurationThings) {
            if (isIterableProperty(item)) {
                resolveIterablePropertyTo(project, collection, item)
            } else {
                switch (item) {
                    case Configuration:
                        collection.add((Configuration) item)
                        break
                    case Map:
                        collection.addAll(asConfigurations(project, (Iterable) ((Map) item).values()))
                        break
                    case Iterable:
                        collection.addAll(asConfigurations(project, (Iterable) item))
                        break
                    case Provider:
                        resolveSingleItemOrIterableTo(project, collection, ((Provider) item).get())
                        break
                    case Callable:
                        resolveSingleItemOrIterableTo(project, collection, ((Callable) item).call())
                        break
                    default:
                        collection.add(asConfiguration(project, item))
                }
            }
        }
        collection
    }

    @CompileDynamic
    static private boolean isIterableProperty(Object o) {
        isListProperty(o) || isSetProperty(o)
    }

    @CompileDynamic
    static private boolean isListProperty(Object o) {
        if (PRE_4_3) {
            false
        } else {
            o instanceof org.gradle.api.provider.ListProperty
        }
    }

    @CompileDynamic
    static private boolean isSetProperty(Object o) {
        if (PRE_4_5) {
            false
        } else {
            o instanceof org.gradle.api.provider.SetProperty
        }
    }

    @CompileDynamic
    static private void resolveIterablePropertyTo(Project project, List<Configuration> configurations, Object o) {
        configurations.addAll(fileize(project, o.get()))
    }

    static private void resolveSingleItemOrIterableTo(
        Project project,
        List<Configuration> configurations,
        Object thingy
    ) {
        switch (thingy) {
            case Configuration:
                configurations.add((Configuration) thingy)
                break
            case Iterable:
                configurations.addAll(asConfigurations(project, (Iterable) thingy))
                break
            default:
                configurations.addAll(asConfigurations(project, [thingy]))
        }
    }
}
