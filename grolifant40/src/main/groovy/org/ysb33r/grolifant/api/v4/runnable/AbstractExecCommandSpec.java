/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant.api.core.ProjectOperations;
import org.ysb33r.grolifant.api.v4.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize;

abstract public class AbstractExecCommandSpec<T extends AbstractExecSpec<T>>
        extends AbstractExecSpec<T> implements ExecutableCommandExecSpec<T> {

    /**
     * The command used in this specification as a String.
     *
     * @return Command
     */
    @Override
    public Provider<String> getCommand() {
        return this.commandProvider;
    }

    /**
     * Set the command to use.
     *
     * @param cmd Anything that can be resolved via {@link StringUtils#stringize(Object)}
     * @return {@code this}
     */
    @Override
    public T setCommand(Object cmd) {
        this.cmd = cmd;
        return (T) this;
    }

    /**
     * Replace the command-specific arguments with a new set.
     *
     * @param args New list of command-specific arguments
     * @return {@code this}
     */
    @Override
    public T setCmdArgs(Iterable<?> args) {
        this.cmdArgs.clear();
        for (Object i : args) {
            this.cmdArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Add more command-specific arguments.
     *
     * @param args Additional list of arguments
     */
    @Override
    public void cmdArgs(Iterable<?> args) {
        for (Object i : args) {
            this.cmdArgs.add(i);
        }
    }

    /**
     * Add more command-specific arguments.
     *
     * @param args Additional list of arguments
     */
    @Override
    public void cmdArgs(Object... args) {
        for (Object i : args) {
            this.cmdArgs.add(i);
        }
    }

    /**
     * Any arguments specific to the command in use
     *
     * @return Arguments to the command. Can be empty, but never null.
     */
    @Override
    public Provider<List<String>> getCmdArgs() {
        return this.cmdArgsProvider;
    }

    protected AbstractExecCommandSpec(ProjectOperations projectOperations) {
        super(projectOperations);
        this.cmdArgs = new ArrayList<Object>();
        this.commandProvider = getProjectOperations().provider(() -> stringize(this.cmd));
        this.cmdArgsProvider = getProjectOperations().provider(() -> stringize(this.cmdArgs));
    }

    @Override
    protected List<String> buildCommandLine() {
        List<String> args = new ArrayList<String>();
        args.add(getExecutable());
        args.addAll(getExeArgs().get());
        args.add(getCommand().get());
        args.addAll(getCmdArgs().get());
        return args;
    }


    private final List<Object> cmdArgs;
    private final Provider<List<String>> cmdArgsProvider;
    private final Provider<String> commandProvider;
    private Object cmd;
}
