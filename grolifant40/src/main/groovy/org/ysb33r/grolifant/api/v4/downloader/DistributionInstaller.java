/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.downloader;

import org.gradle.api.Transformer;
import org.gradle.api.provider.Provider;

import java.io.File;
import java.net.URI;

/**
 * Interface for a downloader and unpacker of distributions.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface DistributionInstaller {

    /**
     * Creates a download URI from a given distribution version
     *
     * @param version Version of the distribution to download
     * @return URI to the where the package is located one
     */
    URI uriFromVersion(final String version);

    /**
     * Set candidate name for SdkMan if the latter should be searched for installed versions
     *
     * @param sdkCandidateName SDK Candidate name. This is the same names that will be shown when
     *                         running {@code sdk list candidates} on the script-line.
     */
    void setSdkManCandidateName(final String sdkCandidateName);

    /**
     * SDKman candidate name for distribution.
     *
     * @return Name of SDKman candidate. Can be {@code null}.
     */
    String getSdkManCandidateName();

    /**
     * Add patterns for files to be marked executable.
     * <p>
     * Calling this method multiple times simply appends for patterns
     *
     * @param relPaths One or more ANT-stype include patterns
     */
    void addExecPattern(String... relPaths);

    /**
     * Set a checksum that needs to be verified against downloaded archive
     *
     * @param cs SHA-256 Hex-encoded checksum
     */
    void setChecksum(final String cs);

    /**
     * Returns the location which is the top or home folder for a distribution.
     * <p>
     * If the distribution is not local, this will invoke a download. Implementations of this function should take care
     * to cache the download as to not unnecessarily use bandwidth and slow builds down.
     *
     * @param version The version that will require locating or downloading.
     * @return Location of the distribution.
     */
    Provider<File> getDistributionRoot(String version);

    /**
     * Locates a file within the distribution
     *
     * @param version     Version of distribution to search.
     * @param fileRelPath Relative path to the distribution root.
     * @return Location of the file in the distribution.
     */
    Provider<File> getDistributionFile(String version, String fileRelPath);

    /**
     * Provide alternative means to look for distributions.
     *
     * @param version Version of distribution to locate
     * @return Location of distribution or null if none were found.
     */
    default File locateDistributionInCustomLocation(String version) {
        return null;
    }

    /**
     * Sets a download root directory for the distribution.
     * <p>
     * If not supplied the default is to use the Gradle User Home.
     * This method is provided for convenience and is mostly only used for testing
     * purposes.
     * <p>
     * The folder will be created at download time if it does not exist.
     *
     * @param downloadRootDir Any writeable directory on the filesystem.
     */
    void setDownloadRoot(Object downloadRootDir);

    /**
     * Sets the method by which to verify an artifact root where package has been unpacked to.
     *
     * @param arv Verifier
     */
    void setArtifactRootVerification(ArtifactRootVerification arv);

    /**
     * Sets the method by which to unpack a downloaded archive.
     * It could also be a way to copy a single downloaded file to a location.
     *
     * @param unpacker Unpack mechanism.
     */
    void setArtifactUnpacker(ArtifactUnpacker unpacker);
}
