/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;


import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Internal;
import org.gradle.process.BaseExecSpec;
import org.gradle.process.ProcessForkOptions;
import org.ysb33r.grolifant.internal.v4.ProviderHelpers;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import static java.util.Collections.EMPTY_LIST;

/**
 * @param <T> A class that implements the {@link Executable interface}
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface Executable<T extends BaseExecSpec> extends ProcessForkOptions, BaseExecSpec {

    /**
     * Add an additional provider that will complement any values set via {@link #environment}.
     *
     * When {@link #getEnvironmentProvider} is realized, additional providers will be called first, thus
     * values set via {@link #environment} will be definitive.
     *
     * @param envProvider
     *
     * @since 1.2.0
     */
    void addEnvironmentProvider(Provider<Map<String,String>> envProvider);

    /**
     * Adds arguments for the executable to be executed.
     *
     * @param args Executable arguments
     * @return {@code this}
     */
    T exeArgs(Iterable<?> args);

    /**
     * Adds arguments for the executable to be executed.
     *
     * @param args Executable arguments
     * @return {@code this}
     */
    T exeArgs(Object... args);

    /**
     * Copies these options to the given target options.
     *
     * @param target Another implementation {@link ProcessForkOptions}.
     * @return {@code this}
     */
    T copyTo(ProcessForkOptions target);

    /**
     * Adds an environment variable to the environment for this process.
     *
     * @param name  Environment variable name
     * @param value Environment variable value
     * @return {@code this}
     */
    T environment(String name, Object value);

    /**
     * Adds some environment variables to the environment for this process.
     *
     * @param environmentVariables Map of environment variables
     * @return {@code this}
     */
    T environment(Map<String, ?> environmentVariables);

    /**
     * Sets the name of the executable to use.
     *
     * @param executable Name of executable. This could be resovled to a {@link String}, {@link File}
     *                   or {@link java.nio.file.Path} before execution
     * @return {@code this}
     */
    T executable(Object executable);

    /**
     * Returns the arguments for the executable to be executed.
     *
     * @return Arguments resolved to strings
     */
    Provider<List<String>> getExeArgs();

    /**
     * Returns the full command line, including the executable plus its arguments.
     *
     * @return Command-line as separate elements in order. Never {@code null}.
     */
    @Internal
    default List<String> getCommandLine() {
        return ProviderHelpers.getOrElse(getCommandLineProvider(), EMPTY_LIST);
    }

    /**
     * Returns the full command line, including the executable plus its arguments.
     *
     * @return Command-line as separate elements in order. Never {@code null}, but list can be empty.
     */
    Provider<List<String>> getCommandLineProvider();

    /**
     * The environment variables to use for the process.
     *
     * @return Environment, but not yet resolved from original values.
     */
    Map<String, Object> getEnvironment();

    /**
     * The environment variables to use for the process.
     *
     * @return Provider to the environment, with values resovled to string
     */
    Provider<Map<String, String>> getEnvironmentProvider();

    /**
     * Returns the output stream to consume standard error from the process executing the command.
     *
     * @return Error stream.
     */
    OutputStream getErrorOutput();

    /**
     * Returns the name of the executable to use.
     *
     * @return Executable resolved to a string including the path if available.
     * Returns {@code null} if no executable provider has been declared
     */
    @Internal
    default String getExecutable() {
        return getExecutableProvider().getOrNull();
    }

    /**
     * Returns the name of the executable to use.
     *
     * @return Executable resolved to a string including the path if available.
     */
    Provider<String> getExecutableProvider();

    /**
     * Returns the standard input stream for the process executing the command.
     *
     * @return Standard input stream.
     */
    InputStream getStandardInput();

    /**
     * Returns the output stream to consume standard output from the process executing the command.
     *
     * @return Standard output stream.
     */
    OutputStream getStandardOutput();

    /**
     * Returns the working directory for the process.
     *
     * @return Provider to a working directory which was resolved to a {@link File}.
     */
    @Internal
    default File getWorkingDir() {
        return getWorkingDirProvider().get();
    }

    /**
     * Returns the working directory for the process.
     *
     * @return Provider to a working directory which was resolved to a {@link File}.
     */
    Provider<File> getWorkingDirProvider();

    /**
     * Tells whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @return {@code true} if return code is ignored.
     */
    boolean isIgnoreExitValue();

    /**
     * Sets the arguments for the command to be executed.
     *
     * @param arguments Arguments can be resolved to string.
     * @return {@code this}.
     */
    T setExeArgs(Iterable<?> arguments);

    /**
     * Sets the arguments for the command to be executed.
     *
     * @param arguments Arguments can be resolved to string.
     * @return {@code this}.
     */
    T setExeArgs(List<String> arguments);

    /**
     * Replaces the existing environment variables to use for the process with a new collection.
     *
     * @param environmentVariables Environment variables
     */
    void setEnvironment(Map<String, ?> environmentVariables);

    /**
     * Sets the output stream to consume standard error from the process executing the command.
     *
     * @param outputStream Redirect error output to this stream.
     * @return {@code this}.
     */
    BaseExecSpec setErrorOutput(OutputStream outputStream);

    /**
     * Sets the name of the executable to use.
     * <p>
     * This will be resolved to a {@link File}, {@link String} or {@link java.nio.file.Path} before execution.
     * </p>
     *
     * @param executable Executable, optionally including a path.
     */
    void setExecutable(Object executable);

    /**
     * Sets whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @param ignoreExitValue {@code true} to prevent exception from being thrown on error.
     * @return {@code this}.
     */
    BaseExecSpec setIgnoreExitValue(boolean ignoreExitValue);

    /**
     * Sets the standard input stream for the process executing the command.
     *
     * @param inputStream Redirect input to this stream.
     * @return {@code this}.
     */
    BaseExecSpec setStandardInput(InputStream inputStream);

    /**
     * Sets the output stream to consume standard output from the process executing the command.
     *
     * @param outputStream Redirect standard output to this stream.
     * @return {@code this}.
     */
    BaseExecSpec setStandardOutput(OutputStream outputStream);

    /**
     * Sets the output stream to consume standard output from the process executing the command.
     *
     * @param outputStream Redirect standard output to this stream.
     */
    default void standardOutput(OutputStream outputStream) {
        setStandardOutput(outputStream);
    }

    /**
     * Sets the working directory for the process.
     *
     * <p>Directory will be resolved to a {@link File} or {@link java.nio.file.Path} before execution.</p>
     *
     * @param dir Working directory
     */
    void setWorkingDir(Object dir);
}
