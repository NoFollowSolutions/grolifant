/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.process.ProcessForkOptions;

import java.io.File;
import java.util.List;

/**
 * Interface for executables which run scripts.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ExecutableScript<T extends Executable<T>> extends Executable<T> {

    /**
     * Whether the script's location should be validates prior to execution.
     *
     * @return {@code true} if validation is required.
     */
    boolean isValidateScriptLocation();

    /**
     * The script used in this specification as a String.
     *
     * @return Script resolved as a string
     */
    Provider<String> getScript();

    /**
     * The script used in this specification as a File.
     *
     * This requires the file to exist.
     *
     * @return Script resolved as a {@link File}.
     */
    Provider<File> getScriptAsFile();

    /**
     * Set the script to use.
     *
     * @param script Anything that can be resolved via {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize(Object)}
     * @return {@code this}
     */
    T setScript(Object script);

    /**
     * Groovy DSL-friendly version.
     *
     * @param scr  Anything that can be resolved via {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize(Object)}
     * @return {@code this}
     */
    default T script(Object scr) {
        return setScript(scr);
    }

    /**
     * Replace the script-specific arguments with a new set.
     *
     * @param args New list of command-specific arguments
     * @return {@code this}
     */
    T setScriptArgs(Iterable<?> args);

    /**
     * Add more script-specific arguments.
     *
     * @param args Additional list of arguments
     */
    void scriptArgs(Iterable<?> args);

    /**
     * Add more script-specific arguments.
     *
     * @param args Additional list of arguments
     */
    void scriptArgs(Object... args);

    /**
     * Any arguments specific to the command in use
     *
     * @return Arguments to the command. Can be empty, but never null.
     */
    Provider<List<String>> getScriptArgs();
}
