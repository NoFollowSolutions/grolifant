/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.ysb33r.grolifant.api.errors.ExecutionException;
import org.ysb33r.grolifant.api.v4.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize;

abstract public class AbstractExecScriptTask<T extends AbstractExecTask<T> & Executable<T>>
        extends AbstractExecTask<T> implements ExecutableScript<T> {

    /**
     * Whether the script's location should be validates prior to execution.
     *
     * @return {@code true} if validation is required.
     */
    @Override
    public boolean isValidateScriptLocation() {
        return this.validateScriptLocation;
    }

    public void setValidateScriptLocation(boolean flag) {
        this.validateScriptLocation = flag;
    }

    /**
     * The script used in this specification as a String.
     *
     * @return Script resovled as a string
     */
    @Override
    @Input
    public Provider<String> getScript() {
        return this.scriptProvider;
    }

    /**
     * The script used in this specification as a File.
     * <p>
     * This requires the file to exist at the time the provider is resolved,
     *
     * @return Script resolved as a {@link File}.
     */
    @Override
    @Internal
    public Provider<File> getScriptAsFile() {
        return this.fileProvider;
    }

    /**
     * Set the script to use.
     *
     * @param script Anything that can be resolved via {@link StringUtils#stringize(Object)}
     * @return {@code this}
     */
    @Override
    public T setScript(Object script) {
        this.script = script;
        return (T) this;
    }

    /**
     * Replace the script-specific arguments with a new set.
     *
     * @param args New list of command-specific arguments
     * @return {@code this}
     */
    @Override
    public T setScriptArgs(Iterable<?> args) {
        this.scriptArgs.clear();
        for (Object i : args) {
            this.scriptArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Add more script-specific arguments.
     *
     * @param args Additional list of arguments
     */
    @Override
    public void scriptArgs(Iterable<?> args) {
        for (Object i : args) {
            this.scriptArgs.add(i);
        }
    }

    /**
     * Add more script-specific arguments.
     *
     * @param args Additional list of arguments
     */
    @Override
    public void scriptArgs(Object... args) {
        for (Object i : args) {
            this.scriptArgs.add(i);
        }
    }

    /**
     * Any arguments specific to the command in use
     *
     * @return Arguments to the command. Can be empty, but never null.
     */
    @Override
    public Provider<List<String>> getScriptArgs() {
        return this.scriptArgsProvider;
    }

    protected AbstractExecScriptTask() {
        super();
        this.scriptArgs = new ArrayList<Object>();
        this.scriptProvider = getProject().provider(() -> stringize(this.script));
        this.scriptArgsProvider = getProject().provider(() -> stringize(this.scriptArgs));
        this.fileProvider = getProjectOperations().map(this.scriptProvider, s -> {
            try {
                File scr = getProjectOperations().file(s).getCanonicalFile();
                if (!scr.exists()) {
                    throw new FileNotFoundException(scr.getCanonicalPath());
                }
                return scr;
            } catch (IOException e) {
                throw new ExecutionException(e.getMessage(), e);
            }
        });
    }

    @Override
    protected List<String> buildCommandLine() {
        List<String> args = new ArrayList<String>();
        args.add(getExecutable());
        args.addAll(getExeArgs().get());
        if (isValidateScriptLocation()) {
            args.add(getScriptAsFile().get().getPath());
        } else {
            args.add(getScript().get());
        }

        args.addAll(getScriptArgs().get());
        return args;
    }

    private final List<Object> scriptArgs;
    private final Provider<List<String>> scriptArgsProvider;
    private final Provider<String> scriptProvider;
    private final Provider<File> fileProvider;
    private Object script;
    private boolean validateScriptLocation = true;
}
