/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.provider.Provider;

import java.util.List;

/**
 * Interface for executables which uses commands.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ExecutableWithCommand<T extends Executable<T>> extends Executable<T> {

    /**
     * The command used in this specification as a String.
     *
     * @return Command
     */
    Provider<String> getCommand();

    /**
     * Set the command to use.
     *
     * @param cmd Anything that can be resolved via {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize(Object)}
     * @return {@code this}
     */
    T setCommand(Object cmd);

    /**
     * Set the command to use.
     *
     * THis provides a more DSL-friendly method.
     *
     * @param cmd Anything that can be resolved via {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize(Object)}
     * @return {@code this}
     */
    default T command(Object cmd) {
        return setCommand(cmd);
    }

    /**
     * Replace the command-specific arguments with a new set.
     *
     * @param args New list of command-specific arguments
     * @return {@code this}
     */
    T setCmdArgs(Iterable<?> args);

    /**
     * Add more command-specific arguments.
     *
     * @param args Additional list of arguments
     */
    void cmdArgs(Iterable<?> args);

    /**
     * Add more command-specific arguments.
     *
     * @param args Additional list of arguments
     */
    void cmdArgs(Object... args);

    /**
     * Any arguments specific to the command in use
     *
     * @return Arguments to the command. Can be empty, but never null.
     */
    Provider<List<String>> getCmdArgs();
}
