/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.downloader

import groovy.transform.InheritConstructors
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.errors.DistributionFailedException

@InheritConstructors
abstract class AbstractSingleFileInstaller extends AbstractDistributionInstaller {

    /** Returns the path to the {@code terraform} executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code terraform} or null if not a supported operating system.
     */
    Provider<File> getSingleFile(String version) {
        projectOperations.map(getDistributionRoot(version)) { File root ->
            root == null ? null : new File(root, singleFileName)
        }
    }

    /** Gets the name of the single file as it should be on local disk
     *
     * @return File name.
     */
    abstract protected String getSingleFileName()

    /** Validates that the downloaded file is good.
     *
     *
     * @param distDir Directory where the file was downloaded to.
     * @return The directory where the real files now exists.
     *
     * @throw {@link DistributionFailedException} if distribution failed to
     *   meet criteria.
     */
    @Override
    protected File verifyDistributionRoot(File distDir) {
        File checkFor = new File(distDir, singleFileName)

        if (!checkFor.exists()) {
            throw new DistributionFailedException(
                "${checkFor.name} not found in downloaded ${distributionName} distribution."
            )
        }

        distDir
    }
}
