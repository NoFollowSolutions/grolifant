/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

/**
 * Interface that specifies an execution specification for an executable that is command-based.
 *
 * @param <T> The class that implements this interface
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ExecutableCommandExecSpec<T extends ExecutableExecSpec<T>>
        extends ExecutableExecSpec<T>, ExecutableWithCommand<T> {

}
