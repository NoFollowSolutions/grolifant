/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ConfigurationException
import org.ysb33r.grolifant.api.errors.ExecutionException
import org.ysb33r.grolifant.api.v4.CombinedProjectTaskExtensionBase
import org.ysb33r.grolifant.internal.v4.Transform

import java.util.concurrent.Callable
import java.util.function.Function
import java.util.regex.Pattern

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/**
 * Use as a base class for extensions that will wrap tools.
 *
 * <p> This base class will also enable extensions to discover whether they are inside a task or a
 * project.
 *
 * @param <T >       THe extension class that extends this base class
 */
@CompileStatic
abstract class AbstractToolExtension<T extends AbstractToolExtension> extends CombinedProjectTaskExtensionBase<T> {

    /**
     * A provider for a resolved executable.
     *
     * @return File provider.
     */
    Provider<File> getExecutable() {
        this.executableProvider
    }

    /**
     * Locate an executable by version, probably downloading it if not local.
     *
     * @param version Something resolvable to a string.
     */
    void executableByVersion(Object version) {
        this.version = version
        this.path = null
        this.searchPath = null
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /**
     * Locate an executable by a local path.
     *
     * @param path Something resolvable to a {@link File}.
     */
    void executableByPath(Object path) {
        this.version = null
        this.path = path
        this.searchPath = null
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /**
     * Locate executable by searching the current environmental search path.
     *
     * @param baseName The base name of the executable
     */
    void executableBySearchPath(Object baseName) {
        this.version = null
        this.path = null
        this.searchPath = baseName
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /**
     * When searching for executables, the order in which to use check for file extensions.
     *
     * @param order List of extensions.
     */
    void setWindowsExtensionSearchOrder(Iterable<String> order) {
        this.windowSearchOrder.clear()
        this.windowSearchOrder.addAll(order)
    }

    void setWindowsExtensionSearchOrder(String... order) {
        this.windowSearchOrder.clear()
        this.windowSearchOrder.addAll(order)
    }

    /**
     * The order in which extensions will be checked for locating an executable on Windows.
     *
     * The default is to use whatever is in the {@code PATHEXT} environmental variable.
     *
     * @return
     */
    List<String> getWindowsExtensionSearchOrder() {
        this.windowSearchOrder ?: (projectExtension ? projectExtension.windowSearchOrder : [])
    }

    /**
     * If configured by version returns that, otherwise it might run the executable to obtain the
     * version.
     *
     * @return Provider to a version string.
     */
    Provider<String> resolvedExecutableVersion() {
        this.executableVersionProvider
    }

    /** Attach this extension to a project
     *
     * @param projectOperations ProjectOperations instance.
     */
    protected AbstractToolExtension(ProjectOperations projectOperations) {
        super(projectOperations)
        this.windowSearchOrder = initWindowsSearchOrder()
        this.executableProvider = initExecutableResolver(projectOperations)
        this.executableVersionProvider = initExecutableVersionProvider(projectOperations)
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to.
     * @param projectOperations ProjectOperations instance.
     * @param projectExtName Name of the extension that is attached to the project.
     */
    protected AbstractToolExtension(Task task, ProjectOperations projectOperations, T projectExtension) {
        super(task, projectOperations, projectExtension)
        if (projectExtension == null) {
            this.windowSearchOrder = initWindowsSearchOrder()
        }
        this.executableProvider = initExecutableResolver(projectOperations)
        this.executableVersionProvider = initExecutableVersionProvider(projectOperations)
    }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link ExecUtils#parseVersionFromOutput} as a helper to implement this method.
     *
     * @return Version string.
     * @throws ConfigurationException if configuration is not correct or version could not be determined.
     */
    abstract protected String runExecutableAndReturnVersion() throws ConfigurationException

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     */
    abstract protected ExecutableDownloader getDownloader()

    /**
     * Resolves the version if it has been set.
     *
     * @return Version string or {@code null}.
     */
    protected String executableVersionOrNull() {
        if (task && noGlobalExecSearch || !task) {
            this.version ? stringize(this.version) : null
        } else if (task && projectExtension) {
            projectExtension.executableVersionOrNull()
        } else {
            failNoExecConfig()
        }
    }

    /**
     * If a path has been set instead of a version, resolve the path.
     *
     * @return Path to executable or {@code null}
     */
    protected File executablePathOrNull() {
        if (task && noGlobalExecSearch || !task) {
            this.path ? projectOperations.file(path).canonicalFile : null
        } else if (task && projectExtension) {
            projectExtension.executablePathOrNull()
        } else {
            failNoExecConfig()
        }
    }

    /**
     * If a search path has been set, resolve the location by searching path
     *
     * @return Path to executable or {@code null} if search path was not set
     */
    protected File executableSearchPathOrNull() {
        if (task && noGlobalExecSearch || !task) {
            if (searchPath) {
                def baseName = stringize(searchPath)
                File found
                if (IS_WINDOWS) {
                    for (String target : Transform.toList(
                        windowSearchOrder,
                        { "${baseName}${it}".toString() } as Function<String, String>
                    )) {
                        found = OperatingSystem.current().findInPath(target)
                        if (found) {
                            break
                        }
                    }
                } else {
                    found = OperatingSystem.current().findInPath(baseName)
                }
                if (found == null) {
                    throw new ExecutionException("Could not locate ${baseName} in search path")
                } else {
                    return found
                }
            } else {
                null
            }
        } else if (task && projectExtension) {
            projectExtension.executableSearchPathOrNull()
        } else {
            failNoExecConfig()
        }
    }

    private File resolveExecutable() {
        if (version) {
            downloader.getByVersion(stringize(version))
        } else if (path) {
            executablePathOrNull()
        } else if (searchPath) {
            executableSearchPathOrNull()
        } else {
            null
        }
    }

    private Provider<String> initExecutableVersionProvider(ProjectOperations po) {
        po.provider(new Callable<String>() {
            @Override
            String call() throws Exception {
                executableVersionOrNull() ?: runExecutableAndReturnVersion()
            }
        })
    }

    private Provider<File> initExecutableResolver(ProjectOperations po) {
        Task t = task
        AbstractToolExtension pe = (AbstractToolExtension) projectExtension
        po.provider(new Callable<File>() {
            @Override
            File call() throws Exception {
                if (t && noGlobalExecSearch) {
                    resolveExecutable()
                } else if (t && pe) {
                    pe.resolveExecutable()
                } else if (t && !pe) {
                    failNoExecConfig()
                } else {
                    resolveExecutable()
                }
            }
        })
    }

    private void failNoExecConfig() {
        throw new ConfigurationException('No method was configured to locate an executable')
    }

    private static List<String> initWindowsSearchOrder() {
        if (IS_WINDOWS) {
            String path = System.getenv('PATHEXT')
            final List<String> entries = []
            if (path != null) {
                for (String entry : path.split(Pattern.quote(OperatingSystem.current().pathSeparator))) {
                    entries.add(entry)
                }
            } else {
                entries.addAll('.COM', '.EXE', '.BAT', '.CMD')
            }
            entries
        } else {
            null
        }
    }

    private final List<String> windowSearchOrder
    private final Provider<File> executableProvider
    private final Provider<String> executableVersionProvider
    private Object version
    private Object path
    private Object searchPath
    private boolean noGlobalExecSearch = false

    private static final boolean IS_WINDOWS = OperatingSystem.current().windows
}
