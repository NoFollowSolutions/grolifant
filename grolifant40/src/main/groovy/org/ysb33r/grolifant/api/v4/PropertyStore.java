/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4;

import org.gradle.api.Incubating;
import org.gradle.api.Project;
import org.gradle.api.Transformer;
import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant.internal.v4.ActualProperty;
import org.ysb33r.grolifant.internal.v4.FakeProperty;
import org.ysb33r.grolifant.api.core.LegacyLevel;

import java.util.NoSuchElementException;

/**
 * Use PropertyStore when you need to deal with Property types, but you also need to be compatible with Gradle 4.0-4.2.
 * <p>
 * Do not expose {@code PropertyStore} in public APIs. If you need access on tasks on extensions, expose the value as a
 * {@link Provider} using the {@link #getAsProvider()} method.
 *
 * @param <T> Type that will used for the property.
 * @since 0.18.0
 */
public interface PropertyStore<T> extends Provider<T> {
    /**
     * Create an instance within a project context. This method should be called during the configuration phase.
     *
     * @param clazz   Property class.
     * @param project Projext context.
     * @param <S>     Property type.
     * @return Property instance.
     */
    static <S> PropertyStore<S> create(Class<S> clazz, Project project) {
        if (LegacyLevel.PRE_4_3) {
            return new FakeProperty<S>(project.getProviders());
        } else {
            return new ActualProperty<S>(project.getObjects(), clazz);
        }
    }

    /**
     * Create an instance within a project context.
     * <p>
     * This method should be called during the configuration phase.
     *
     * @param value   Value to initialise the property with.
     * @param project Property context
     * @param <S>     Property type.
     * @return Property instance.
     */
    static <S> PropertyStore<S> create(S value, Project project) {
        if (LegacyLevel.PRE_4_3) {
            return new FakeProperty<S>(project.getProviders(), value);
        } else {
            return new ActualProperty<S>(project.getObjects(), value);
        }
    }

    /**
     * Returns the actual implementation object
     *
     * @return Internal implementation object. THis is dependant on the version of Gradle.
     */
    @Incubating
    Object getAsProperty();

    /**
     * Return this as a provider.
     *
     * @return Provider to the specific type
     */
    Provider<T> getAsProvider();

    /**
     * Set a fixed value for the property.
     *
     * @param t Property value.
     */
    void set(T t);

    /**
     * Proxy to property valuer to another provider.
     *
     * @param provider Provider to use.
     */
    void set(Provider<? extends T> provider);

    /**
     * Returns the current value if it exists.
     *
     * @return Stored value
     * @throws NoSuchElementException
     */
    T get();

    /**
     * Returns the value if it exists, otherwise null
     *
     * @return Value or null.
     */
    T getOrNull();

    /**
     * Returns the value if it exists, otherwise return the provided value.
     *
     * @param t Alternative value.
     * @return Existing or alternative value.
     */
    T getOrElse(T t);

    /**
     * Map this provider to another provider.
     *
     * @param transformer Transforming function
     * @param <S>         Type returned by new provider.
     * @return Mapped provider.
     */
    <S> Provider<S> map(Transformer<? extends S, ? super T> transformer);

    /**
     * Checks whether there is a value present.
     * If the property is a prooxy to another provider. That provider is checked.
     *
     * @return {@code true} if value is present.
     */
    boolean isPresent();
}

