/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Transformer
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.ProviderTools
import org.ysb33r.grolifant.internal.v4.ProviderHelpers

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3

/**
 * Safely deal with Providers down to Gradle 4.0.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
class DefaultProviderTools implements ProviderTools {

    DefaultProviderTools(ProviderFactory providerFactory) {
        this.providerFactory = providerFactory
    }

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @param defaultValue Default value of provider does not have a vlue
     * @return Provider value or default vlue.
     */
    @Override
    public <T> T getOrElse(Provider<T> provider, T defaultValue) {
        ProviderHelpers.getOrElse(provider, defaultValue)
    }

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @return Provider value or {@code null}.
     */
    @Override
    public <T> T getOrNull(Provider<T> provider) {
        ProviderHelpers.getOrNull(provider)
    }

    /**
     * Allow flatMap functionality for providers even before Gradle 5.0.
     *
     * @param provider Existing provider.
     * @param transformer Transform one provider to another.
     * @param <S >     Return type of new provider.
     * @param <T >     Return type of existing provider.
     * @return New provider.
     *
     * @since 1.2
     */
    @Override
    public <S, T> Provider<S> flatMap(
        Provider<T> provider,
        Transformer<? extends Provider<? extends S>, ? super T> transformer
    ) {
        providerFactory.provider { ->
            if (provider.present) {
                def providerValue = provider.get()
                def transformedValue = transformer.transform(providerValue)
                getOrNull(transformedValue)
            } else {
                null
            }
        }
    }

    /** Maps one provider to another.
     * Use this method if you need to support Gradle 4.0 - 4.2 as part of compatibility.
     *
     * @param base Original provider
     * @param transformer Transforming function.
     * @return New provider
     *
     * @since 1.2
     */
    @Override
    public <IN, OUT> Provider<OUT> map(Provider<IN> base, Transformer<OUT, IN> transformer) {
        if (PRE_4_3) {
            mapImplPreGradle43(base, transformer)
        } else {
            mapImpl(base, transformer)
        }
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     *
     * Returns a Provider whose value is the value of this provider, if present, otherwise the given default value.
     *
     * @param provider Original provider.
     * @param value The default value to use when this provider has no value.
     * @param <T >   Provider type.
     * @return Provider value or default value.
     *
     * @since 1.2
     */
    @Override
    public <T> Provider<T> orElse(Provider<T> provider, T value) {
        providerFactory.provider { ->
            getOrElse(provider, value)
        }
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * Returns a Provider whose value is the value of this provider, if present, otherwise uses the value from
     * the given provider, if present.
     *
     * @param provider Original provider
     * @param elseProvider The provider whose value should be used when this provider has no value.
     * @param <T >   Provider type
     * @return Provider chain.
     *
     * @since 1.2
     */
    @Override
    public <T> Provider<T> orElse(Provider<T> provider, Provider<? extends T> elseProvider) {
        providerFactory.provider { ->
            provider.present ? provider.get() : elseProvider.get()
        }
    }

    @CompileDynamic
    private <IN, OUT> Provider<OUT> mapImplPreGradle43(
        Provider<IN> provider,
        Transformer<OUT, IN> transformer
    ) {
        providerFactory.provider({ ->
            transformer.transform(ProviderHelpers.getOrNull(provider))
        } as Callable<OUT>)
    }

    private <IN, OUT> Provider<OUT> mapImpl(
        Provider<IN> provider,
        Transformer<OUT, IN> transformer
    ) {
        provider.map(transformer)
    }

    private final ProviderFactory providerFactory
}
