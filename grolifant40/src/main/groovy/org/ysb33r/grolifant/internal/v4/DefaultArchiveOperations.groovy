/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy

/**
 * Legacy operations to deal with archive operations in a way that
 * will allow forward compatibility with Gradle 6.6+.
 *
 * @since 1.0.0
 */
@CompileStatic
class DefaultArchiveOperations implements ArchiveOperationsProxy {

    DefaultArchiveOperations(Project project) {
        this.project = project
    }

    @Override
    FileTree tarTree(Object tarPath) {
        project.tarTree(tarPath)
    }

    @Override
    FileTree zipTree(Object zipPath) {
        project.zipTree(zipPath)
    }

    private final Project project
}
