/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4.wrappers;

import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant.api.core.LegacyLevel;

import java.io.File;

/**
 * Wraps a file property.
 *
 * @since 1.3
 */
public interface FilePropertyWrapper {

    static FilePropertyWrapper create(Project project) {
        if(LegacyLevel.PRE_4_3) {
            return new FilePropertyPre43(project);
        } else {
            return new FileProperty(project);
        }
    }

    /**
     * Gets the file referenced by this property.
     *
     * @return File instance.
     */
    File getFile();

    /**
     * Gets the provider for this file property
     *
     * @return File provider
     */
    Provider<File> getFileProvider();

    /**
     * Sets a lazy-evaluated file.
     *
     * @param file
     */
    void setFile(Object file);
}
