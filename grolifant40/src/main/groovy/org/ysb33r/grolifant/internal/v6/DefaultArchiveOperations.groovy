/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v6

import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.internal.project.ProjectInternal
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy
import org.ysb33r.grolifant.api.core.LegacyLevel

import java.util.function.Function

/**
 * Provides an implementation of {@link org.gradle.api.file.ArchiveOperations} that works with Gradle 6.0+.
 *
 * On Gradle < 6.6 it will use the equivalent operations on the {@link Project} object.
 * On Gradle 6.6+ it will use the provided {@link org.gradle.api.file.ArchiveOperations} service.
 *
 * @since 1.0.0
 */
class DefaultArchiveOperations implements ArchiveOperationsProxy {

    DefaultArchiveOperations(Project project) {
        if (LegacyLevel.PRE_6_6) {
            zip = { Project p, Object f -> project.zipTree(f) }.curry(project)
            tar = { Project p, Object f -> project.tarTree(f) }.curry(project)
        } else {
            def services = findServices(project)
            zip = services[0]
            tar = services[1]
        }
    }

    @Override
    FileTree tarTree(Object tarPath) {
        tar.apply(tarPath)
    }

    @Override
    FileTree zipTree(Object zipPath) {
        zip.apply(zipPath)
    }

    private static List<Function<Object, FileTree>> findServices(Project project) {
        def service = ((ProjectInternal) project).services.get(
            DefaultArchiveOperations.classLoader.loadClass('org.gradle.api.file.ArchiveOperations')
        )
        [
            { Object f -> service.zipTree(f) } as Function<Object, FileTree>,
            { Object f -> service.tarTree(f) } as Function<Object, FileTree>
        ]
    }

    private final Function<Object, FileTree> zip
    private final Function<Object, FileTree> tar

}
