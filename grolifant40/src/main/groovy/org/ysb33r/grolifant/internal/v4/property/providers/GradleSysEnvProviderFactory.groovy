/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4.property.providers

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.GradleSysEnvProvider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.internal.v4.ProviderHelpers

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_6
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_6_6
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.internal.v4.property.order.StandardPropertyResolveOrders.ProjectSystemEnvironment

/**
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1.0
 */
@CompileStatic
class GradleSysEnvProviderFactory {
    static GradleSysEnvProvider loadInstance(Project project, ProjectOperations projectOperations) {
        if (PRE_6_6) {
            new PreGradle66(projectOperations, project.properties)
        } else {
            new Gradle66(projectOperations)
        }
    }

    private static Provider<String> provide(
        ProviderFactory providerFactory,
        final Provider<String> propertyValue,
        final Object defaultValue
    ) {
        if (defaultValue != null) {
            valueOrElse(
                providerFactory,
                propertyValue,
                providerFactory.provider { -> stringize(defaultValue) }
            )
        } else {
            propertyValue
        }
    }

    @CompileDynamic
    static private Provider<String> valueOrElse(
        ProviderFactory providerFactory,
        Provider<String> provider,
        final Provider<String> defaultValue
    ) {
        if (PRE_5_6) {
            providerFactory.provider { ->
                ProviderHelpers.getOrElse(provider, defaultValue)
            }
        } else {
            provider.orElse(defaultValue)
        }
    }

    private static class Gradle66 implements GradleSysEnvProvider {
        Gradle66(ProjectOperations projectOperations) {
            this.providerFactory = projectOperations.providers
            this.projectOperations = projectOperations
        }

        @CompileDynamic
        @Override
        Provider<String> systemProperty(Object name, boolean configTime) {
            def provider = this.providerFactory.systemProperty(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @CompileDynamic
        @Override
        Provider<String> gradleProperty(Object name, boolean configTime) {
            def provider = this.providerFactory.gradleProperty(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @CompileDynamic
        @Override
        Provider<String> environmentVariable(Object name, boolean configTime) {
            def provider = this.providerFactory.environmentVariable(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @Override
        Provider<String> resolve(Object name, Object defaultValue, boolean configTime) {
            provide(
                this.providerFactory,
                resolver.resolve(projectOperations, stringize(name), configTime),
                defaultValue
            )
        }

        private final ProjectOperations projectOperations
        private final ProviderFactory providerFactory
        private final ProjectSystemEnvironment resolver = new ProjectSystemEnvironment()
    }

    private static class PreGradle66 implements GradleSysEnvProvider {
        PreGradle66(ProjectOperations projectOperations, Map<String, ?> props) {
            this.providerFactory = projectOperations.providers
            this.projectOperations = projectOperations
            this.projectProperties = props
            this.systemProperties = System.properties
        }

        @Override
        Provider<String> systemProperty(Object name, boolean configTime) {
            this.providerFactory.provider({ Map<String, ?> map ->
                stringize(map[stringize(name)])
            }.curry(this.systemProperties) as Callable<String>)
        }

        @Override
        Provider<String> gradleProperty(Object name, boolean configTime) {
            this.providerFactory.provider({ Map<String, ?> props ->
                stringize(props[stringize(name)])
            }.curry(this.projectProperties) as Callable<String>)
        }

        @Override
        Provider<String> environmentVariable(Object name, boolean configTime) {
            this.providerFactory.provider { ->
                System.getenv(stringize(name))
            }
        }

        @Override
        Provider<String> resolve(Object name, Object defaultValue, boolean configTime) {
            provide(
                this.providerFactory,
                resolver.resolve(projectOperations, stringize(name), configTime),
                defaultValue
            )
        }

        private final ProjectOperations projectOperations
        private final ProviderFactory providerFactory
        private final Map<String, ?> projectProperties
        private final Properties systemProperties
        private final ProjectSystemEnvironment resolver = new ProjectSystemEnvironment()
    }

}
