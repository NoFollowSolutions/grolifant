/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Transformer
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.v4.PropertyStore

import java.util.concurrent.Callable
import java.util.function.BiFunction

/**
 * Allows for a Fake Property implementation on Gradle 4.0 - 4.2
 *
 * @since 1.0.0
 */
@CompileStatic
class FakeProperty<T> implements PropertyStore<T> {

    @CompileDynamic
    FakeProperty(ProviderFactory pf) {
        value = []
        providerFactory = pf
        provider = pf.provider(makeCallable())
    }

    @CompileDynamic
    FakeProperty(ProviderFactory pf, T v1) {
        value = [v1]
        providerFactory = pf
        provider = pf.provider(makeCallable())
    }

    T get() {
        T ret = valueOrProvider
        if (ret == null) {
            throw new NoSuchElementException('No value present')
        }
        valueOrProvider
    }

    void set(T v) {
        if (value.empty) {
            value.add(v)
            providerExt = null
        } else {
            value[0] = v
        }
    }

    @Override
    Object getAsProperty() {
        this
    }

    @Override
    Provider<T> getAsProvider() {
        this.provider
    }

    @Override
    void set(Provider<? extends T> provider) {
        this.providerExt = provider
        value.clear()
    }

    @Override
    T getOrNull() {
        valueOrProvider
    }

    @Override
    T getOrElse(T t) {
        valueOrProvider ?: t
    }

    @Override
    Provider<T> orElse(T value) {
        present ? this : providerFactory.provider { -> value }
    }

    @Override
    Provider<T> orElse(Provider<? extends T> provider) {
        present ? this : provider
    }

    @Override
    @CompileDynamic
    public <S> Provider<S> map(Transformer<? extends S, ? super T> transformer) {
        providerFactory.provider(makeTransformerCallable(transformer))
    }

    @Override
    def <S> Provider<S> flatMap(Transformer<? extends Provider<? extends S>, ? super T> transformer) {
        transformer.transform(valueOrProvider) as Provider<S>
    }

    @Override
    boolean isPresent() {
        value.empty ? providerExt?.present : true
    }

    /**
     * This is a NOP as {@link FakeProperty} is only used where configuration cache is not available.
     *
     * @return The current provider.
     */
    @Override
    Provider<T> forUseAtConfigurationTime() {
        provider
    }

    @Override
    def <B, R> Provider<R> zip(Provider<B> provider2, BiFunction<T, B, R> biFunction) {
        map { T value ->
            biFunction.apply(value, provider2.get())
        }
    }

    private T getValueOrProvider() {
        value.empty ? (providerExt?.present ? providerExt.get() : null) : value[0]
    }

    private <S> Callable<S> makeTransformerCallable(Transformer<? extends S, ? super T> transformer) {
        new Callable<S>() {
            @Override
            S call() throws Exception {
                transformer.transform(provider.get())
            }
        }
    }

    private Callable<T> makeCallable() {
        new Callable<T>() {
            @Override
            T call() throws Exception {
                valueOrProvider
            }
        }
    }

    private final List<T> value
    private final Provider<T> provider
    private final ProviderFactory providerFactory
    private Provider<? extends T> providerExt
}
