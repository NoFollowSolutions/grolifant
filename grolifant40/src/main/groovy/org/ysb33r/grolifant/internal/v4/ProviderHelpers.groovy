/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.LegacyLevel

/**
 * Utilities for dealing for Provider incompatibilities across Gradle versions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
class ProviderHelpers {

    /**
     * Allow getOrElse functionality for providers even before Gradle 4.3.
     *
     * @param provider Provider
     * @param defaultValue Default value of provider does not have a vlue
     * @return Provider value or default vlue.
     */
    @CompileDynamic
    public static <T> T getOrElse(Provider<T> provider, T defaultValue) {
        if (LegacyLevel.PRE_4_3) {
            provider.present ? provider.get() : defaultValue
        } else {
            provider.getOrElse(defaultValue)
        }
    }

    /**
     *
     * @param provider
     * @return
     */
    @CompileDynamic
    public static <T> T getOrNull(Provider<T> provider) {
        if (LegacyLevel.PRE_4_3) {
            provider.present ? provider.get() : null
        } else {
            provider.getOrNull()
        }
    }
}
