/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ConfigurationException
import org.ysb33r.grolifant.api.v4.downloader.AbstractDistributionInstaller
import org.ysb33r.grolifant.api.v4.downloader.DistributionInstaller
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecCommandSpec
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecWrapperTask
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecWrapperWithExtensionTask
import org.ysb33r.grolifant.api.v4.runnable.AbstractToolExtension
import org.ysb33r.grolifant.api.v4.runnable.ExecMethods
import org.ysb33r.grolifant.api.v4.runnable.ExecUtils
import org.ysb33r.grolifant.api.v4.runnable.ExecutableDownloader
import org.ysb33r.grolifant.api.v4.runnable.ProvisionedExecMethods
import org.ysb33r.grolifant.compatibility.testing.helpers.ExecHelperSpecification

@SuppressWarnings(['PublicMethodsBeforeNonPublicMethods', 'UnnecessaryGetter'])
class RunnablePackageExecWrapperTaskSpec extends ExecHelperSpecification {

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    void setup() {
        projectOperations = ProjectOperations.create(project)
    }

    void 'Run a command-line tool as a task with a project extension'() {
        setup:
        project.allprojects {
            apply plugin: MyPlugin

            // tag::configure-the-tool-on-project-extension[]
            toolConfig {
                executableByVersion('1.2.3') // <1>
                executableBySearchPath('mycmd') // <2>
                executableByPath('/usr/local/bin/mycmd')// <3>
            }
            // end::configure-the-tool-on-project-extension[]
        }

        // The reason the value is set again is because the previous two are
        // merely for coverage and documentation purposes.
        final String testScriptPath = ExecHelperSpecification.scriptToPass.absolutePath
        project.allprojects {
            toolConfig {
                executableByPath(testScriptPath)
            }
        }

        when:
        project.evaluate()
        def cmdLine = project.tasks.mycmd.fakeRun()

        then:
        cmdLine == [testScriptPath, 'show-colours', '--yellow', '--bright']
    }

    void 'Run a command-line tool as a task with a project & task extension'() {
        setup:
        project.allprojects {
            apply plugin: MyPlugin2

            // tag::configure-the-tool-on-task-extension[]
            mycmd {
                toolConfig { // <1>
                    executableByVersion('1.2.3')
                    executableBySearchPath('mycmd')
                    executableByPath('/usr/local/bin/mycmd')
                }
            }
            // end::configure-the-tool-on-task-extension[]
        }

        // The reason the value is set again is because the previous two are
        // merely for coverage and documentation purposes.
        final String testScriptPath = ExecHelperSpecification.scriptToPass.absolutePath
        project.allprojects {
            mycmd {
                toolConfig {
                    executableByPath(testScriptPath)
                }
            }
        }

        when:
        project.evaluate()
        def cmdLine = project.tasks.mycmd.fakeRun()

        then:
        cmdLine == [testScriptPath, 'show-colours', '--yellow', '--bright']
    }

    void 'Run exec from project extension'() {
        setup:
        project.allprojects {
            apply plugin: MyPlugin3

            toolConfig {
                executableByVersion('1.2.3')
                executableBySearchPath('mycmd')
                executableByPath(ExecHelperSpecification.scriptToPass.absolutePath)
            }
            // tag::make-task-call-extension1[]
            tasks.create('runMe') {
                doLast {
                    toolConfig.exec {
                        command = 'show-colours'
                        cmdArgs '--yellow', '--bright'
                    }
                }
            }
            // end::make-task-call-extension1[]
        }

        when:
        project.evaluate()
        ExecResult result = project.toolConfig.exec {
            command = 'show-colours'
            cmdArgs '--yellow', '--bright'
        }

        then:
        result.exitValue == 0
    }

    void 'Run exec from project extension - provisioned variant'() {
        setup:
        project.allprojects {
            apply plugin: MyPlugin4

            toolConfig {
                executableByVersion('1.2.3')
                executableBySearchPath('mycmd')
                executableByPath(ExecHelperSpecification.scriptToPass.absolutePath)
            }
            // tag::make-task-call-extension1[]
            tasks.create('runMe') {
                doLast {
                    toolConfig.exec {
                        command = 'show-colours'
                        cmdArgs '--yellow', '--bright'
                    }
                }
            }
            // end::make-task-call-extension1[]
        }

        when:
        project.evaluate()
        project.toolConfig.exec {
            command = 'show-colours'
            cmdArgs '--yellow', '--bright'
        }

        then:
        noExceptionThrown()
    }

    void 'Run exec from project extension - downloaded package'() {
        setup:
        final String DISTVER = ExecHelperSpecification.DISTVER
        project.allprojects {
            apply plugin: MyPlugin5

            toolConfig {
                executableByVersion(DISTVER)
            }
        }

        when:
        project.evaluate()
        project.toolConfig.exec {
            command = 'show-colours'
            cmdArgs '--yellow', '--bright'
        }

        then:
        noExceptionThrown()
    }

    static
    // tag::example-execspec[]
    class MyCmdExecSpec extends AbstractExecCommandSpec<MyCmdExecSpec> {
        MyCmdExecSpec(ProjectOperations po) {
            super(po)
        }
    }
    // end::example-execspec[]

    static
    // tag::example-extension[]
    class MyExtension extends AbstractToolExtension<MyExtension> { // <1>

        public static final String NAME = 'toolConfig'

        MyExtension(Project project) { // <2>
            super(ProjectOperations.find(project))
        }

        MyExtension(Task task) {
            super( // <3>
                task,
                ProjectOperations.find(task.project),
                task.project.extensions.getByName(NAME)
            )
            // end::example-extension[]
            // tag::example-extension[]
        }

        @Override
        protected ExecutableDownloader getDownloader() { // <4>
            new ExecutableDownloader() {
                File getByVersion(String version) {
                    toolDownloader.getDistributionFile(version, FILENAME)
                }
            }
        }

        @Override
        protected String runExecutableAndReturnVersion() throws ConfigurationException {
            ExecUtils.parseVersionFromOutput(
                projectOperations,
                ['-v'],
                executable.get(),
                { String output ->
                    output.readLines()[0].repalceFirst('version: ', '')
                }
            )
        }

        private static final String FILENAME = OperatingSystem.current().windows ? 'test.bat' : 'test.sh'
        private final DistributionInstaller toolDownloader // <5>
    }
    // end::example-extension[]

    static
    // tag::example-extension-exec[]
    class MyExtensionWithExec extends AbstractToolExtension<MyExtension>
        implements ExecMethods<MyCmdExecSpec> { // <1>

        public static final String NAME = 'toolConfig'

        MyExtensionWithExec(Project project) {
            super(ProjectOperations.find(project))
        }

        MyExtensionWithExec(Task task) {
            super(
                task,
                ProjectOperations.find(task.project),
                task.project.extensions.getByName(NAME)
            )
        }

        @Override
        ExecResult exec(Action<MyCmdExecSpec> specConfigurator) { // <2>
            MyCmdExecSpec spec = new MyCmdExecSpec(projectOperations) // <3>
            spec.executable(getExecutable()) // <4>
            specConfigurator.execute(spec) // <5>
            exec(spec) // <6>
        }

        @Override
        ExecResult exec(MyCmdExecSpec spec) { // <7>
            projectOperations.exec(new Action<ExecSpec>() { // <8>
                @Override
                void execute(ExecSpec execSpec) {
                    spec.copyToExecSpec(execSpec)
                }
            })
        }

        @Override
        protected ExecutableDownloader getDownloader() {
            new ExecutableDownloader() {
                File getByVersion(String version) {
                    toolDownloader.getDistributionFile(version, FILENAME)
                }
            }
        }

        @Override
        protected String runExecutableAndReturnVersion() throws ConfigurationException {
            // end::example-extension-exec[]
            ExecUtils.parseVersionFromOutput(
                projectOperations,
                ['-v'],
                executable.get(),
                { String output ->
                    output.readLines()[0].replaceFirst('version: ', '')
                }
            )
            // tag::example-extension-exec[]
            /* ... */
        }

        private static final String FILENAME = OperatingSystem.current().windows ? 'test.bat' : 'test.sh'
        private final DistributionInstaller toolDownloader
    }
    // end::example-extension-exec[]

    static
    // tag::example-extension-provisioned[]
    class MyExtensionWithProvisionedExec extends AbstractToolExtension<MyExtension>
        implements ProvisionedExecMethods<MyCmdExecSpec> { // <1>

        public static final String NAME = 'toolConfig'

        MyExtensionWithProvisionedExec(Project project) {
            super(ProjectOperations.find(project))
        }

        MyExtensionWithProvisionedExec(Task task) {
            super(
                task,
                ProjectOperations.find(task.project),
                task.project.extensions.getByName(NAME)
            )
        }

        @Override
        MyCmdExecSpec createExecSpec() { // <2>
            MyCmdExecSpec spec = new MyCmdExecSpec(projectOperations)
            spec.executable(getExecutable()) // <3>
            spec
        }

        @Override
        protected ExecutableDownloader getDownloader() {
            // end::example-extension-provisioned[]
            new ExecutableDownloader() {
                File getByVersion(String version) {
                    toolDownloader.getDistributionFile(version, FILENAME)
                }
            }
            // tag::example-extension-provisioned[]
            /* ... */
        }

        @Override
        protected String runExecutableAndReturnVersion() throws ConfigurationException {
            // end::example-extension-provisioned[]
            ExecUtils.parseVersionFromOutput(
                projectOperations,
                ['-v'],
                executable.get(),
                { String output ->
                    output.readLines()[0].replaceFirst('version: ', '')
                }
            )
            // tag::example-extension-provisioned[]
            /* ... */
        }

        private static final String FILENAME = OperatingSystem.current().windows ? 'test.bat' : 'test.sh'
        private final DistributionInstaller toolDownloader
    }
    // end::example-extension-provisioned[]

    static
    // tag::example-extension-downloader[]
    class MyExtensionWithDownloader extends AbstractToolExtension<MyExtension>
        implements ProvisionedExecMethods<MyCmdExecSpec> {

        public static final String NAME = 'toolConfig'

        MyExtensionWithDownloader(Project project) {
            super(ProjectOperations.find(project))
            this.toolDownloader = new MyInstaller(projectOperations)
        }

        MyExtensionWithDownloader(Task task) {
            super(
                task,
                ProjectOperations.find(task.project),
                task.project.extensions.getByName(NAME)
            )
            this.toolDownloader = new MyInstaller(projectOperations)
        }

        @Override
        MyCmdExecSpec createExecSpec() {
            MyCmdExecSpec spec = new MyCmdExecSpec(projectOperations)
            spec.executable(getExecutable())
            spec
        }

        @Override
        protected ExecutableDownloader getDownloader() {
            def dnl = toolDownloader
            new ExecutableDownloader() {
                File getByVersion(String version) {
                    dnl.getDistributionFile(version, FILENAME).get()
                }
            }
        }

        @Override
        protected String runExecutableAndReturnVersion() throws ConfigurationException {
            // end::example-extension-downloader[]
            ExecUtils.parseVersionFromOutput(
                projectOperations,
                ['-v'],
                executable.get(),
                { String output ->
                    output.readLines()[0].replaceFirst('version: ', '')
                }
            )
            // tag::example-extension-downloader[]
            /* ... */
        }

        private static final String FILENAME = OperatingSystem.current().windows ? 'test.bat' : 'test.sh'
        private final DistributionInstaller toolDownloader
    }
    // end::example-extension-downloader[]

    static
    // tag::example-task[]
    class MyWrapperTask extends AbstractExecWrapperTask<MyCmdExecSpec> { // <1>

        MyWrapperTask() {
            super()
            myExtension = project.extensions.getByType(MyExtension) // <2>
        }

        @Override
        protected File getExecutableLocation() {
            myExtension.executable.get() // <3>
        }

        @Override
        protected MyCmdExecSpec createExecSpec() {
            new MyCmdExecSpec(projectOperations) // <4>
        }

        @Override
        protected void configureExecSpec(MyCmdExecSpec execSpec) { // <5>
            execSpec.executable = executableLocation // <6>
            execSpec.command = 'show-colours'
            execSpec.cmdArgs '--yellow', '--bright'
        }

        private final MyExtension myExtension

        // end::example-task[]
        List<String> fakeRun() {
            def execSpec = createExecSpec()
            configureExecSpec(execSpec)
            execSpec.commandLine
        }
        // tag::example-task[]
    }
    // end::example-task[]

    static
    // tag::example-task2[]
    class MyWrapperExtTask extends AbstractExecWrapperWithExtensionTask<MyExtension, MyCmdExecSpec> { // <1>

        MyWrapperExtTask() {
            super()
            myExtension = extensions.create( // <2>
                MyExtension.NAME,
                MyExtension,
                this
            )
        }

        @Override
        protected File getExecutableLocation() {
            toolExtension.executable?.get()
        }

        @Override
        protected MyExtension getToolExtension() { // <3>
            myExtension
        }

        @Override
        protected MyCmdExecSpec createExecSpec() {
            new MyCmdExecSpec(projectOperations)
        }

        @Override
        protected void configureExecSpec(MyCmdExecSpec execSpec) {
            execSpec.executable = executableLocation
            execSpec.command = 'show-colours'
            execSpec.cmdArgs '--yellow', '--bright'
        }

        private final MyExtension myExtension

        // end::example-task2[]
        List<String> fakeRun() {
            def execSpec = createExecSpec()
            configureExecSpec(execSpec)
            execSpec.commandLine
        }
        // tag::example-task2[]
    }
    // end::example-task2[]

    static
    // tag::example-plugin[]
    class MyPlugin implements Plugin<Project> {
        void apply(Project project) {
            ProjectOperations.maybeCreateExtension(project) // <1>
            project.extensions.create(MyExtension.NAME, MyExtension, project) // <2>
            project.tasks.create('mycmd', MyWrapperTask) // <3>
        }
    }
    // end::example-plugin[]

    static
    class MyPlugin2 implements Plugin<Project> {
        // tag::example-plugin2[]
        void apply(Project project) {
            ProjectOperations.maybeCreateExtension(project)
            project.extensions.create(MyExtension.NAME, MyExtension, project)
            project.tasks.create('mycmd', MyWrapperExtTask)
        }
        // end::example-plugin2[]
    }

    static
    class MyPlugin3 implements Plugin<Project> {
        void apply(Project project) {
            ProjectOperations.maybeCreateExtension(project)
            project.extensions.create(MyExtension.NAME, MyExtensionWithExec, project)
        }
    }

    static
    class MyPlugin4 implements Plugin<Project> {
        void apply(Project project) {
            ProjectOperations.maybeCreateExtension(project)
            project.extensions.create(MyExtension.NAME, MyExtensionWithProvisionedExec, project)
        }
    }

    static
    class MyPlugin5 implements Plugin<Project> {
        void apply(Project project) {
            ProjectOperations.maybeCreateExtension(project)
            project.extensions.create(MyExtension.NAME, MyExtensionWithDownloader, project)
        }
    }

    static
    // tag::example-downloader[]
    class MyInstaller extends AbstractDistributionInstaller {

        MyInstaller(ProjectOperations projectOperations) {
            super(
                'Test Distribution',  // <1>
                'native-binaries/testdist', // <2>
                projectOperations
            )
            addExecPattern('**/*.sh', '**/*.bat') // <3>
        }

        @Override
        URI uriFromVersion(String version) { // <4>
            // Make a downloadable URL from a version string
            // end::example-downloader[]
            TESTDIST_DIR.toURI().resolve("testdist-${DISTVER}.zip")
            // tag::example-downloader[]
        }

        // end::example-downloader[]
        private static final File TESTDIST_DIR = ExecHelperSpecification.TESTDIST_DIR
        // tag::example-downloader[]
    }
    // end::example-downloader[]

}