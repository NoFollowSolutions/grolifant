/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.internal.UncheckedException
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ExecutionException
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecCommandTask
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecScriptTask
import spock.lang.Specification
import spock.lang.Unroll

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0

class RunnablePackageExecTaskSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    void setup() {
        ProjectOperations.maybeCreateExtension(project)
    }

    void 'Instantiate exec-type task'() {
        setup:
        project.tasks.create('mycmd', TerraformExec) {
            command 'install'
            cmdArgs 'some.pkg'
        }

        when:
        project.evaluate()
        def cmdLine = project.tasks.mycmd.fakeRun()

        then:
        verifyAll {
            cmdLine == ['terraform', 'install', 'some.pkg']
        }
    }

    @Unroll
    void 'Instantiate script-type task where #text'() {
        setup:
        project.file('install.py').text = ''
        project.tasks.create('scriptor', PythonExec) {
            script 'install.py'
            scriptArgs 'a', 'b'
            validateScriptLocation = validated
        }

        when:
        project.evaluate()
        def cmdLine = project.tasks.scriptor.fakeRun()

        then:
        verifyAll {
            cmdLine == ['python', validated ? project.file('install.py').absolutePath : 'install.py', 'a', 'b']
        }

        where:
        validated | text
        true      | 'script location is validated'
        false     | 'script location is not validated'
    }

    void 'Instantiate script-type task where script does not exist and validation is required'() {
        setup:
        project.tasks.create('scriptor', PythonExec) {
            script 'install.py'
            scriptArgs 'a', 'b'
            validateScriptLocation = true
        }

        when:
        project.evaluate()
        project.tasks.scriptor.scriptAsFile.get()

        then:
        thrown(e)

        where:
        e = PRE_4_3 ? UncheckedException : ExecutionException
    }

    @SuppressWarnings('UnnecessarySetter')
    static
    // tag::example-tool-exec-type[]
    class TerraformExec extends AbstractExecCommandTask<TerraformExec> {
        TerraformExec() {
            super()
            setExecutable('terraform')
        }
        // end::example-tool-exec-type[]
        List<String> fakeRun() {
            buildCommandLine()
        }
        // tag::example-tool-exec-type[]
    }
    // end::example-tool-exec-type[]

    @SuppressWarnings('UnnecessarySetter')
    static
    // tag::example-script-exec-type[]
    class PythonExec extends AbstractExecScriptTask<PythonExec> {
        PythonExec() {
            super()
            setExecutable('python')
        }

        // end::example-script-exec-type[]
        List<String> fakeRun() {
            buildCommandLine()
        }
        // tag::example-script-exec-type[]
    }
    // end::example-script-exec-type[]
}