/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.exec.AbstractCommandExecSpec
import org.ysb33r.grolifant.api.v4.exec.AbstractCommandExecTask
import org.ysb33r.grolifant.api.v4.exec.AbstractScriptExecSpec
import org.ysb33r.grolifant.api.v4.exec.AbstractScriptExecTask
import org.ysb33r.grolifant.api.v4.exec.ResolverFactoryRegistry
import org.ysb33r.grolifant.api.v4.exec.ToolExecSpec
import org.ysb33r.grolifant.compatibility.testing.helpers.ExecHelperSpecification
import spock.lang.Ignore

@Deprecated
class AbstractExecTaskSpec extends ExecHelperSpecification {

    static final File scriptToPass = ExecHelperSpecification.scriptToPass

    @SuppressWarnings('UnnecessarySetter')
    static
    // end::example-tool-exec-spec[]
    class MyCmdExecSpec extends AbstractCommandExecSpec {
        MyCmdExecSpec(Project project, File defaultBinary) {
            super(ProjectOperations.find(project), new ResolverFactoryRegistry(project))
            setExecutable(defaultBinary)
        }
        // end::example-tool-exec-spec[]
        @Override
        ToolExecSpec configure(Action<? extends ToolExecSpec> action) {
            null
        }
        // tag::example-tool-exec-spec[]
    }
    // end::example-tool-exec-spec[]

    @SuppressWarnings('UnnecessarySetter')
    static
    // tag::example-tool-exec-type[]
    class MyCmdExec extends AbstractCommandExecTask<MyCmdExecSpec> {
        MyCmdExec() {
            super()
            // end::example-tool-exec-type[]
            setToolExecutable(AbstractExecTaskSpec.scriptToPass)
            // tag::example-tool-exec-type[]
        }

        @Override
        MyCmdExecSpec createExecSpec() {
            new MyCmdExecSpec(project, new File('/bin/mycmd'))
        }

        MyCmdExecSpec fakeRun() {
            createAndSetExecSpec()
            configureExecSpec()
            execSpec
        }
    }
    // end::example-tool-exec-type[]

    @SuppressWarnings('UnnecessarySetter')
    static
    class MyScriptExecSpec extends AbstractScriptExecSpec {
        MyScriptExecSpec(Project project, File defaultBinary) {
            super(ProjectOperations.find(project), new ResolverFactoryRegistry(project))
            setExecutable(defaultBinary)
        }

        @Override
        ToolExecSpec configure(Action<? extends ToolExecSpec> action) {
            null
        }
    }

    static
    class MyScriptExec extends AbstractScriptExecTask<MyScriptExecSpec> {
        MyScriptExec() {
            super()
            toolExecutable = scriptToPass
        }

        @Override
        MyScriptExecSpec createExecSpec() {
            new MyScriptExecSpec(project, new File('/bin/mycmd'))
        }

        MyScriptExecSpec fakeRun() {
            createAndSetExecSpec()
            configureExecSpec()
            execSpec
        }
    }

    Project project = ProjectBuilder.builder().build()

    void setup() {
        ProjectOperations.maybeCreateExtension(project)
    }

    @Ignore
    void 'Instantiate exec-type task'() {
        setup:
        project.tasks.create('mycmd', MyCmdExec) {
            command 'install'
            cmdArgs 'some.pkg'
        }

        when:
        project.evaluate()
        MyCmdExecSpec execSpec = project.tasks.mycmd.fakeRun()

        then:
        verifyAll {
            execSpec.commandLine.toList() == ['/bin/cmd', 'install', 'some.pkg']
        }
    }

    @Ignore
    void 'Instantiate script-type task'() {
        setup:
        project.tasks.create('scriptor', MyScriptExec) {
            script 'install.py'
            scriptArgs 'a', 'b'
        }

        when:
        project.evaluate()
        MyScriptExecSpec execSpec = project.tasks.scriptor.fakeRun()

        then:
        verifyAll {
            execSpec.commandLine.toList() == ['/bin/cmd', 'install.py', 'a', 'b']
        }
    }
}