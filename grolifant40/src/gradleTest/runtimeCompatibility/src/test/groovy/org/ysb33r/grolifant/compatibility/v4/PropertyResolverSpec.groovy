/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.api.internal.project.DefaultProject
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.PropertyResolver
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.environment.RestoreSystemProperties

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_6_6
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_7_1
import static org.ysb33r.grolifant.internal.v4.property.order.PropertyNaming.asEnvVar

@RestoreSystemProperties
class PropertyResolverSpec extends Specification {

    public static final String SYSPROP_TO_SET_TO_EMPTY = 'this.sysprop.is.empty'

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations = ProjectOperations.maybeCreateExtension(project)

    // tag::create-property-resolver[]
    PropertyResolver resolver = new PropertyResolver(project)
    // end::create-property-resolver[]

    void setupSpec() {
        System.setProperty(SYSPROP_TO_SET_TO_EMPTY, '')
    }

    void setup() {
        if (PRE_7_1 && !PRE_6_6) {
            ((DefaultProject) project).services.get(org.gradle.initialization.GradlePropertiesController).
                loadGradlePropertiesFrom(project.projectDir)
        }
        project.evaluate()
    }

    void 'Can read a system property'() {
        setup:
        def key = System.getProperties().keySet()[-1]
        def value = System.getProperty(key)

        expect:
        resolver.get(key) == value
    }

    void 'Can read an environment variable'() {
        setup:
        def key = System.getenv().keySet()[-1]
        def value = System.getenv(key)
        def lookFor = key.toLowerCase(Locale.US).replaceAll('_', '.')

        expect:
        resolver.get(lookFor) == value
    }

    void 'Can return null is nothing is defined()'() {
        expect:
        resolver.get('foo.bar.king.kong') == null
    }

    void 'Can return default value if nothing is defined'() {
        expect:
        resolver.get('foo.bar.king.kong', '123') == '123'
    }


    void 'An empty string is not a null value'() {
        expect:
        resolver.get(SYSPROP_TO_SET_TO_EMPTY) == ''
    }

    void 'Can use alternative order'() {
        expect:
        resolver.get('foo.bar.king.kong', resolver.SYSTEM_ENV_PROPERTY) == null
        resolver.get('foo.bar.king.kong', '123', resolver.SYSTEM_ENV_PROPERTY) == '123'
    }

    @Unroll
    void '#value is convertible to environmental variables'() {
        expect:
        asEnvVar(value) == 'A_B_C'

        where:
        value << ['a.b.c', 'a-b-c']
    }
}
