/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.process.ProcessForkOptions
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.StringUtils
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecCommandSpec
import org.ysb33r.grolifant.api.v4.runnable.AbstractExecScriptSpec
import spock.lang.Specification

class RunnablePackageExecSpecificationsSpec extends Specification {

    public static final Boolean IS_WINDOWS = OperatingSystem.current().windows

    Project project
    GitExecSpec commandExecSpec
    PerlScriptExecSpec scriptExecSpec

    void setup() {
        project = ProjectBuilder.builder().build()
        def projectOperations = ProjectOperations.maybeCreateExtension(project)
        commandExecSpec = new GitExecSpec(projectOperations)
        scriptExecSpec = new PerlScriptExecSpec(projectOperations)
    }

    @SuppressWarnings('UnnecessaryObjectReferences')
    void 'Configuring a command specification'() {
        setup:
        def gitPath = IS_WINDOWS ? new File(project.projectDir, '/path/to/git').absolutePath : '/path/to/git'
        def externalEnvironmentProvider = project.provider { ->
            [ foo9: 'bar9', foo: 'notBar']
        }
        when:
        commandExecSpec.with {
            // tag::declarative[]
            ignoreExitValue = true  // <1>
            standardOutput = System.out  // <2>
            standardInput = System.in    // <3>
            errorOutput = System.err     // <4>
            workingDir = '.'     // <5>
            // end::declarative[]

            // tag::environment[]
            environment = [foo: 'bar']               // <1>
            environment foo2: 'bar2', foo3: { 'bar3' } // <2>
            environment 'foo4', 'bar4'   // <3>
            addEnvironmentProvider(externalEnvironmentProvider) // <4>
            // end::environment[]

            // tag::exe[]
            executable { '/path/to/git' }         // <1>
            exeArgs = ['-C', '/path/to/project']     // <2>
            exeArgs '-p', { '--bare' }  // <3>
            // end::exe[]

            // tag::command[]
            command = 'remote'         // <1>
            cmdArgs = ['prune', '--dry-run'] // <2>
            cmdArgs '-n', { 'origin' }    // <3>
            // end::command[]

            executable gitPath
        }

        then:
        commandExecSpec.commandLine == [
            gitPath,
            '-C', '/path/to/project', '-p', '--bare',
            'remote',
            'prune', '--dry-run', '-n', 'origin'
        ]
        commandExecSpec.ignoreExitValue == true
        commandExecSpec.standardInput == System.in
        commandExecSpec.standardOutput == System.out
        commandExecSpec.errorOutput == System.err
        commandExecSpec.workingDir == project.file('.')
        commandExecSpec.environmentProvider.get() == [foo: 'bar', foo2: 'bar2', foo3: 'bar3', foo4: 'bar4', foo9: 'bar9']
    }

    @SuppressWarnings('UnnecessaryObjectReferences')
    void 'Configuring a script specification'() {
        File perl = IS_WINDOWS ? new File(project.projectDir, '/path/to/perl.exe') : new File('/path/to/perl.exe')

        when:
        scriptExecSpec.with {
            executable perl
            workingDir = '.'
            // tag::script-examples[]
            script = 'install.pl'       // <1>
            scriptArgs = ['aye']      // <2>
            scriptArgs 'cee', { 'dee' }  // <3>
            // end::script-examples[]
        }

        then:
        scriptExecSpec.commandLine == [
            perl.absolutePath,
            'install.pl',
            'aye', 'cee', 'dee'
        ]

        scriptExecSpec.ignoreExitValue == false
        scriptExecSpec.standardInput == System.in
        scriptExecSpec.standardOutput == System.out
        scriptExecSpec.errorOutput == System.err
        scriptExecSpec.workingDir == project.file('.')
    }

    void 'Lazy-evaluate command'() {
        when:
        commandExecSpec.command = { 'install' }

        then:
        commandExecSpec.command.get() == 'install'
    }

    void 'Copying process fork options'() {
        setup:
        def exePath = IS_WINDOWS ? new File(project.projectDir, '/path/to/exe').absolutePath : '/path/to/exe'
        ProcessForkOptions target = new ProcessForkOptions() {
            @Override
            String getExecutable() {
                StringUtils.stringize(exe)
            }

            // Override in 4.0
            void setExecutable(String s) {
                this.exe = s
            }

            @Override
            void setExecutable(Object o) {
                this.exe = o
            }

            @Override
            ProcessForkOptions executable(Object o) {
                this.exe = o
                this
            }

            @Override
            File getWorkingDir() {
                project.file(wd)
            }

            @Override
            void setWorkingDir(Object o) {
                this.wd = o
            }

            // Override in 4.0
            void setWorkingDir(File f) {
                this.wd = f
            }

            @Override
            ProcessForkOptions workingDir(Object o) {
                this.wd = o
                this
            }

            @Override
            Map<String, Object> getEnvironment() {
                env
            }

            @Override
            void setEnvironment(Map<String, ?> map) {
                env.clear()
                env.putAll map
            }

            @Override
            ProcessForkOptions environment(Map<String, ?> map) {
                env.putAll map
                this
            }

            @Override
            ProcessForkOptions environment(String s, Object o) {
                env.add s, o
                this
            }

            @Override
            ProcessForkOptions copyTo(ProcessForkOptions processForkOptions) {
                null // Not going to use this in test
            }

            private Object exe
            private Object wd
            private final Map<String, Object> env = [:]
        }

        when:
        commandExecSpec.with {
            executable = exePath
            environment foo: 'bar'
            workingDir = '.'
        }

        commandExecSpec.copyTo(target)

        then:
        target.executable == exePath
        target.workingDir == project.file('.')
        target.environment == [foo: 'bar']
    }

    @SuppressWarnings('UnnecessarySetter')
    static
    // tag::example-command-spec[]
    class GitExecSpec extends AbstractExecCommandSpec<GitExecSpec> {
        GitExecSpec(ProjectOperations projectOperations) {
            super(projectOperations)
            setExecutable('git')
        }
    }
    // end::example-command-spec[]

    @SuppressWarnings('UnnecessarySetter')
    static
    // tag::example-script-spec[]
    class PerlScriptExecSpec extends AbstractExecScriptSpec<PerlScriptExecSpec> {
        PerlScriptExecSpec(ProjectOperations projectOperations) {
            super(projectOperations)
            setExecutable('perl')
        }
    }
    // end::example-script-spec[]
}

