/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic
import org.gradle.api.Project

/** Utilities to deal with unpacking certain formats.
 *
 * @deprecated Use methods from {@link org.ysb33r.grolifant.api.v4.UnpackUtils} instead
 * @since 0.6
 */
@CompileStatic
@Deprecated
class UnpackUtils {

    /** Unpack a DMG image on MacOs.
     *
     * <p> NOOP on other operating systems.
     *
     * @param project Project instance that unpacking is related to.
     * @param tempPrefix A preifx for the temporrary directory that will be used.
     * @param srcArchive DMG archive to unpack
     * @param relPath Relative path within the DMG to unpack.
     * @param destDir Directory to unpack into.
     *
     * @deprecated
     *
     * @since 0.6
     */
    static void unpackDmgOnMacOsX(
        final Project project,
        final String tempPrefix,
        final File srcArchive,
        final String relPath,
        final File destDir
    ) {
        org.ysb33r.grolifant.api.v4.UnpackUtils.unpackDmgOnMacOsX(
            project,
            tempPrefix,
            srcArchive,
            relPath,
            destDir
        )
    }
}
