/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.exec

import groovy.transform.CompileStatic

/** Creates a {@link org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable}.
 *
 * @deprecated Use {@link org.ysb33r.grolifant.api.v4.exec.ResolvedExecutableFactory} instead.
 *
 * @since 0.3
 */
@CompileStatic
@Deprecated
@SuppressWarnings(['LineLength', 'InterfaceNameSameAsSuperInterface'])
interface ResolvedExecutableFactory  {
    ResolvableExecutable build(Map<String, Object> options, Object from)
}
