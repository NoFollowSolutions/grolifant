/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic

/** Methods for dealing with closures.
 *
 * @deprecated Use {@link org.ysb33r.grolifant.api.v4.ClosureUtils}.
 *
 * @since 0.3
 */
@CompileStatic
@Deprecated
class ClosureUtils {

    /** Configure this item using a closure
     *
     * @param item Item to configure
     * @param cfg Configuration closure to use.
     *
     * @deprecated.
     */
    static void configureItem(Object item, Closure cfg) {
        org.ysb33r.grolifant.api.v4.ClosureUtils.configureItem(item, cfg)
    }
}
