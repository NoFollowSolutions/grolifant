/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileCollection
import org.ysb33r.grolifant.api.core.ClassLocation

import java.nio.file.Path
import java.util.regex.Pattern

/** Various file utilities.
 *
 * @deprecated Use methods from {@link org.ysb33r.grolifant.api.v4.FileUtils} instead.
 */
@CompileStatic
@Deprecated
class FileUtils {

    public static final Pattern SAFE_FILENAME_REGEX = org.ysb33r.grolifant.api.v4.FileUtils.SAFE_FILENAME_REGEX

    /** Converts a string into a string that is safe to use as a file name. T
     *
     * The result will only include ascii characters and numbers, and the "-","_", #, $ and "." characters.
     *
     * @param A potential file name
     * @return A name that is safe on the local filesystem of the current operating system.
     * @deprecated
     */
    @CompileDynamic
    static String toSafeFileName(String name) {
        org.ysb33r.grolifant.api.v4.FileUtils.toSafeFileName(name)
    }

    /** Converts a collection of String into a {@link Path} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @deprecated
     *
     * @since 0.8
     */
    static Path toSafePath(String... parts) {
        org.ysb33r.grolifant.api.v4.FileUtils.toSafePath(parts)
    }

    /** Converts a collection of String into a {@@link File} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     *
     * @since 0.8
     *
     * @deprecated
     */
    static File toSafeFile(String... parts) {
        toSafePath(parts).toFile()
    }

    /** Returns the file collection that a {@link CopySpec} describes.
     *
     * @param copySpec An instance of a {@link CopySpec}
     * @return Result collection of files.
     * @deprecated
     */
    static FileCollection filesFromCopySpec(CopySpec copySpec) {
        org.ysb33r.grolifant.api.v4.FileUtils.filesFromCopySpec(copySpec)
    }

    /** Provides a list of directories below another directory
     *
     * @param distDir Directory
     * @return List of directories. Can be empty if, but never {@code null}
     *   supplied directory.
     * @deprecated
     */
    static List<File> listDirs(File distDir) {
        org.ysb33r.grolifant.api.v4.FileUtils.listDirs(distDir)
    }

    /** Returns the classpath location for a specific class
     *
     * @param aClass Class to find.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     *   on classpath
     * @throw ClassNotFoundException* @deprecated
     *
     * @since 0.9
     */
    @SuppressWarnings('DuplicateStringLiteral')
    static ClassLocation resolveClassLocation(Class aClass) {
        org.ysb33r.grolifant.api.v4.FileUtils.resolveClassLocation(aClass)
    }

    /** Returns the project cache directory for the given project.
     *
     * @param project Project to query.
     *
     * @return Project cache directory. Never {@code null}.
     *
     * @deprecated
     *
     * @since 0.14
     */
    static File projectCacheDirFor(Project project) {
        org.ysb33r.grolifant.api.v4.FileUtils.projectCacheDirFor(project)
    }
}