/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.exec;

import groovy.lang.Closure;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.TaskAction;
import org.gradle.process.ExecResult;
import org.gradle.process.ExecSpec;
import org.gradle.process.ProcessForkOptions;
import org.ysb33r.grolifant.api.StringUtils;
import org.ysb33r.grolifant.api.errors.ExecutionException;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.ysb33r.grolifant.api.StringUtils.stringize;

/**
 * A base class to use for developing execution tasks for wrapping execution tools
 *
 * @since 0.3
 * @deprecated Use{@link org.ysb33r.grolifant.api.v4.exec.AbstractExecTask} instead.
 */
@Deprecated
@SuppressWarnings("MethodCount")
public abstract class AbstractExecTask<B extends AbstractExecTask, T extends AbstractToolExecSpec> extends DefaultTask {
    /**
     * Determine whether the exit value should be ignored.
     *
     * @param flag Whether exit value should be ignored.
     * @return {@code this}.
     */
    public B setIgnoreExitValue(boolean flag) {
        this.ignoreExitValue = flag;
        return (B) this;
    }

    /**
     * Determine whether the exit value should be ignored.
     *
     * @param flag Whether exit value should be ignored.
     * @return {@code this}.
     */
    public B ignoreExitValue(boolean flag) {
        this.ignoreExitValue = flag;
        return (B) this;
    }

    /**
     * State of exit value monitoring.
     *
     * @return Whether return value should be ignored.
     */
    @Input
    public boolean isIgnoreExitValue() {
        return this.ignoreExitValue;
    }

    /**
     * Set the stream where standard input should be read from for this process when executing.
     *
     * @param inputStream Inout stream to use.
     * @return {@code this}.
     */
    public B setStandardInput(InputStream inputStream) {
        this.standardInput = inputStream;
        return (B) this;
    }

    /**
     * Set the stream where standard input should be read from for this process when executing.
     *
     * @param inputStream Inout stream to use.
     * @return {@code this}.
     */
    public B standardInput(InputStream inputStream) {
        return setStandardInput(inputStream);
    }

    /**
     * Where input is read from during execution.
     *
     * @return Input stream.
     */
    @Internal
    public InputStream getStandardInput() {
        return this.standardInput;
    }

    /**
     * Set the stream where standard output should be sent to for this process when executing.
     *
     * @param outputStream Output stream to use.
     * @return {@code this}.
     */
    public B setStandardOutput(OutputStream outputStream) {
        this.standardOutput = outputStream;
        return (B) this;
    }

    /**
     * Set the stream where standard output should be sent to for this process when executing.
     *
     * @param outputStream Output stream to use.
     * @return {@code this}.
     */
    public B standardOutput(OutputStream outputStream) {
        return setStandardOutput(outputStream);
    }

    /**
     * Where standard output is sent to during execution.
     *
     * @return Output stream.
     */
    @Internal
    public OutputStream getStandardOutput() {
        return this.standardOutput;
    }

    /**
     * Set the stream where error output should be sent to for this process when executing.
     *
     * @param outputStream Output stream to use.
     * @return {@code this}.
     */
    public B setErrorOutput(OutputStream outputStream) {
        this.errorOutput = outputStream;
        return (B) this;
    }

    /**
     * Set the stream where error output should be sent to for this process when executing.
     *
     * @param outputStream Output stream to use.
     * @return {@code this}.
     */
    public B errorOutput(OutputStream outputStream) {
        return setErrorOutput(outputStream);
    }

    /**
     * Where error output is sent to during execution.
     *
     * @return Output stream.
     */
    @Internal
    public OutputStream getErrorOutput() {
        return this.errorOutput;
    }

    /**
     * Obtain the working directory for this process.
     * <p>
     * This call will evaluate the lazily-set working directory for {@link #setWorkingDir}0
     *
     * @return A{@code java.io.File} object.
     */
    @Internal
    public File getWorkingDir() {
        return getProject().file(this.workingDir);
    }

    /**
     * Set the working directory for the execution.
     *
     * @param workDir Any object that is convertible using Gradle's {@code project.file}.
     */
    public void setWorkingDir(Object workDir) {
        this.workingDir = workDir;
    }

    /**
     * Set the working directory for the execution.
     *
     * @param workDir Any object that is convertible using Gradle's {@code project.file}.
     * @return This object as {@link ProcessForkOptions}
     */
    public B workingDir(Object workDir) {
        this.workingDir = workDir;
        return (B) this;
    }

    /**
     * Returns the environment to be used for the process. Defaults to the environment of this process.
     *
     * @return Key-value pairing of environmental variables.
     */
    @Internal
    public Map<String, Object> getEnvironment() {
        return this.env;
    }

    /**
     * Set the environment variables to use for the process.
     *
     * @param map Environmental variables as key-value pairs.
     */
    public void setEnvironment(Map<String, ?> map) {
        this.env.clear();
        this.env.putAll(map);
    }

    /**
     * Add additional environment variables for use with the process.
     *
     * @param map Environmental variables as key-value pairs.
     * @return {@code this}.
     */
    public B environment(Map<String, ?> map) {
        this.env.putAll(map);
        return (B) this;
    }

    /**
     * Add additional environment variable for use with the process.
     *
     * @param envVar Name of environmental variable.
     * @param value  Value of environmental variable.
     * @return {@code this}.
     */
    public B environment(String envVar, Object value) {
        this.env.put(envVar, value);
        return (B) this;
    }

    /**
     * The exe used in this specification as a String.
     *
     * @return Executable name if set (else null).
     */
    @Optional
    @Input
    public String getExecutable() {
        return stringize(this.exe);
    }

    /**
     * Returns the full script line, including the exe, it's specific arguments, tool specific instruction and
     * the arguments specific to the instruction.
     *
     * @return Command-line as a list of items
     */
    @Internal
    public List<String> getCommandLine() {
        return execSpec != null ? execSpec.getCommandLine() : new ArrayList();
    }

    /**
     * Replace the tool-specific arguments with a new set.
     *
     * @param args New list of tool-specific arguments
     */
    public void setExeArgs(Iterable<?> args) {
        this.args.clear();
        for(Object i : args) {
            this.args.add(i);
        }
    }

    /**
     * Add more tool-specific arguments.
     *
     * @param args Additional list of arguments
     */
    public void exeArgs(Iterable<?> args) {
        for(Object i : args) {
            this.args.add(i);
        }
    }

    /**
     * Add more tool-specific arguments.
     *
     * @param args Additional list of arguments
     */
    public void exeArgs(Object... args) {
        for(Object i : args) {
            this.args.add(i);
        }
    }

    /**
     * Any arguments specific to the tool in use
     *
     * @return Arguments to the tool. Can be empty, but never null.
     */
    @Optional
    @Input
    public List<String> getExeArgs() {
        return stringize(this.args);
    }

    /**
     * Returns the result for the execution, that was run by this task.
     *
     * @return The result of the execution. Returns null if this task has not been executed yet.
     */
    @Internal
    public ExecResult getExecResult() {
        return execResult;
    }

    /**
     * Runs this process against an internal execution specification. If a failure occurs and
     * {@link #isIgnoreExitValue} is not set, an exception will be raised.
     */
    @TaskAction
    @SuppressWarnings("CatchException")
    public void exec() {
        this.execSpec = createExecSpec();
        configureExecSpec();
        Action<ExecSpec> runner = toSpec -> execSpec.copyToExecSpec(toSpec);
        try {
            execResult = getProject().exec(runner);
        } catch (final Exception e) {
            throw new ExecutionException("Failure in running external process", e);
        }

    }

    /**
     * Creates class and sets default environment to be that of Gradle,
     */
    protected AbstractExecTask() {
        super();
        getEnvironment().clear();
        getEnvironment().putAll(System.getenv());
    }

    /**
     * Sets the exe to use for this task
     *
     * @param exe Anything resolvable via {@link StringUtils#stringize(Object)}
     */
    protected void setToolExecutable(Object exe) {
        this.exe = exe;
    }

    /**
     * Provides access to the execution specification that is associated with this task
     *
     * @return Execution specification. Can be null if call too early.
     */
    protected T getExecSpec() {
        return this.execSpec;
    }

    /**
     * Configures the executions specification from settings.
     *
     * @return The execution specification.
     * @since 0.5.1
     */
    protected T configureExecSpec() {
        this.execSpec.setIgnoreExitValue(this.ignoreExitValue);
        this.execSpec.environment(this.env);
        this.execSpec.exeArgs(getExeArgs());

        if (this.standardInput != null) {
            this.execSpec.setStandardInput(this.standardInput);
        }


        if (this.standardOutput != null) {
            this.execSpec.setStandardOutput(this.standardOutput);
        }


        if (this.errorOutput != null) {
            this.execSpec.setErrorOutput(this.errorOutput);
        }


        if (this.workingDir != null) {
            this.execSpec.workingDir(this.workingDir);
        }


        if (this.exe != null) {
            this.execSpec.executable(stringize(this.exe));
        }


        return this.execSpec;
    }

    /**
     * Factory method for creating an execution specification
     *
     * @return Execution Specification
     */
    protected abstract T createExecSpec();

    private T execSpec;
    private ExecResult execResult;
    private boolean ignoreExitValue = false;
    private InputStream standardInput;
    private OutputStream standardOutput;
    private OutputStream errorOutput;
    private Object workingDir;
    private Object exe;
    private final List<Object> args = new ArrayList<Object>();
    private final Map<String, Object> env = new LinkedHashMap<String, Object>();
}
