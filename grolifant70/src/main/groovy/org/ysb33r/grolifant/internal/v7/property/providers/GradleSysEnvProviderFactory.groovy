/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v7.property.providers

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.GradleSysEnvProvider
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.internal.v4.property.order.StandardPropertyResolveOrders.ProjectSystemEnvironment

/**
 * Lightweight wrapper around providers for Gradle & system properties and for environmental variables.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1.0
 */
@CompileStatic
class GradleSysEnvProviderFactory {
    static GradleSysEnvProvider loadInstance(ProjectOperations projectOperations) {
        new Gradle70(projectOperations)
    }

    private static class Gradle70 implements GradleSysEnvProvider {
        Gradle70(ProjectOperations projectOperations) {
            this.providerFactory = projectOperations.providers
            this.projectOperations = projectOperations
        }

        @Override
        Provider<String> systemProperty(Object name, boolean configTime) {
            def provider = this.providerFactory.systemProperty(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @Override
        Provider<String> gradleProperty(Object name, boolean configTime) {
            def provider = this.providerFactory.gradleProperty(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @Override
        Provider<String> environmentVariable(Object name, boolean configTime) {
            def provider = this.providerFactory.environmentVariable(stringize(name))
            configTime ? provider.forUseAtConfigurationTime() : provider
        }

        @Override
        Provider<String> resolve(Object name, Object defaultValue, boolean configTime) {
            Provider<String> propertyValue = resolver.resolve(projectOperations, stringize(name), configTime)

            if (defaultValue != null) {
                propertyValue.orElse(providerFactory.provider { -> stringize(defaultValue) })
            } else {
                propertyValue
            }
        }

        private final ProjectOperations projectOperations
        private final ProviderFactory providerFactory
        private final ProjectSystemEnvironment resolver = new ProjectSystemEnvironment()
    }
}
