/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v7

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.ProviderTools

/**
 * Safely deal with Providers down to Gradle 7.0.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
@InheritConstructors
class DefaultProviderTools implements ProviderTools {

    /**
     * API consistency, but optimized to only work for Gradle 7.0+
     *
     * @param provider Provider
     * @param defaultValue Default value of provider does not have a vlue
     * @return Provider value or default value.
     */
    @Override
    public <T> T getOrElse(Provider<T> provider, T defaultValue) {
        provider.getOrElse(defaultValue)
    }

    /**
     * API consistency, but optimized to only work for Gradle 7.0+
     *
     * @param provider Provider
     * @return Provider value or {@code null}.
     */
    @Override
    public <T> T getOrNull(Provider<T> provider) {
        provider.getOrNull()
    }
}
