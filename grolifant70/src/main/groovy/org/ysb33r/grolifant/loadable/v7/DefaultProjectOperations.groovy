/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v7

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.CopySpec
import org.gradle.api.file.Directory
import org.gradle.api.file.FileCollection
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.file.FileOperations
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy
import org.ysb33r.grolifant.api.core.ExecOperationsProxy
import org.ysb33r.grolifant.api.core.GradleSysEnvProvider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.ProviderTools
import org.ysb33r.grolifant.api.errors.NotSupportedException
import org.ysb33r.grolifant.api.v4.StringUtils
import org.ysb33r.grolifant.api.v5.FileUtils
import org.ysb33r.grolifant.internal.v6.DefaultArchiveOperations
import org.ysb33r.grolifant.internal.v6.DefaultExecOperations
import org.ysb33r.grolifant.internal.v7.property.providers.GradleSysEnvProviderFactory
import org.ysb33r.grolifant.loadable.core.ProjectOperationsProxy
import org.ysb33r.grolifant.loadable.v5.DefaultTaskTools

import java.util.concurrent.Callable
import java.util.function.BiConsumer
import java.util.function.Function

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_7_0
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/**
 * An extension that can be added to a project by a plugin to aid in compatibility
 *
 * @since 1.0.0
 */
@CompileStatic
@Slf4j
class DefaultProjectOperations extends ProjectOperationsProxy {

    final ProviderTools providerTools

    /**
     * Constructor that sets up a number of methods to be compatible across a wide range Gradle releases.
     *
     * @param project
     */
    DefaultProjectOperations(Project project) {
        super(
            project,
            new org.ysb33r.grolifant.loadable.v6.DefaultFileSystemOperations(project),
            new DefaultTaskTools(project.tasks, project.providers)
        )
        if (PRE_7_0) {
            throw new NotSupportedException('This class can only be loaded on Gradle 7.0+')
        }

        this.projectCacheDir = project.gradle.startParameter.projectCacheDir ?:
            project.file("${project.rootDir}/.gradle")
        this.projectLayout = project.layout
        this.providerFactory = project.providers
        this.fileOperations = ((ProjectInternal) project).services.get(FileOperations)
        this.archives = new DefaultArchiveOperations(project)
        this.execs = new DefaultExecOperations(project)
        this.fileTreeFactory = { Object base -> project.objects.fileTree().from(base) }
        this.providerTools = new DefaultProviderTools()
        this.propertyProvider = GradleSysEnvProviderFactory.loadInstance(this)

        this.updateFileProperty = { Provider<File> p, Object f ->
            FileUtils.updateFileProperty(projectLayout, providerFactory, (Property) p, f)
        }
        this.updateStringProperty = { ProjectOperations po, Provider<String> p, Object s ->
            StringUtils.updateStringProperty(po, p, s)
        }.curry(this) as BiConsumer<Provider<String>, Object>

        this.buildDir = projectLayout.buildDirectory.map({ Directory it ->
            it.asFile
        } as Transformer<File, Directory>)

        this.projectVersion = project.provider { -> stringize(project.version ?: '') }
        this.projectGroup = project.provider { -> stringize(project.group ?: '') }
    }

    @Override
    CopySpec copySpec() {
        fileOperations.copySpec()
    }

    /**
     * Converts a collection of file-like objects to a a list of  {@link java.io.File} instances with project context.
     * <p>
     * It will convert anything that the singular version of {@code FileUtils.fileize} can do.
     * In addition it will recursively resolve any collections that result out of resolving the supplied items.
     *
     * @param files List of object to evaluate
     * @return List of resolved files.
     */
    @Override
    List<File> fileize(Iterable<Object> files) {
        FileUtils.fileize(projectLayout, files)
    }

    /**
     * Creates a new ConfigurableFileTree. The tree will have no base dir specified.
     *
     * @param base Base for file tree.
     *
     * @return File tree.
     */
    @Override
    ConfigurableFileTree fileTree(Object base) {
        this.fileTreeFactory.apply(base)
    }

    /**
     * Build directory
     *
     * @return Provider to the build directory
     */
    @Override
    Provider<File> getBuildDir() {
        this.buildDir
    }

    /**
     * Lazy-evaluated project group.
     *
     * @return provider to project group
     */
    @Override
    Provider<String> getGroupProvider() {
        this.projectGroup
    }

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    @Override
    Provider<String> getVersionProvider() {
        this.projectVersion
    }

    /**
     * Safely resolve the stringy items as a path below build directory.
     *
     * @param stringy Any item that can be resolved to a string using
     * {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize}
     * @return Provider to a file
     */
    @Override
    Provider<File> buildDirDescendant(Object stringy) {
        map(this.buildDir, { File it ->
            new File(it, StringUtils.stringize(stringy))
        } as Transformer<File, File>)
    }

    /**
     * <p>Creates a {@link FileCollection} containing the given files, as defined by {@link Project#files(Object ...)}.
     *
     * <p>This method can also be used to create an empty collection, but the collection may not be mutated later.</p>
     *
     * @param paths The paths to the files. May be empty.
     * @return The file collection. Never returns null.
     */
    @Override
    FileCollection files(Object... paths) {
        projectLayout.files(paths)
    }

    /** Returns the project cache dir
     *
     * @return Location of cache directory
     */
    @Override
    File getProjectCacheDir() {
        this.projectCacheDir
    }

    /** Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    @Override
    public <T> Provider<T> provider(Callable<? extends T> var1) {
        providerFactory.provider(var1)
    }

    /** Updates a file provider.
     *
     * Update property
     * otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param stringy Value that should be lazy-resolved.
     */
    @Override
    void updateFileProperty(
        Provider<File> provider,
        Object file
    ) {
        this.updateFileProperty.accept(provider, file)
    }

    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.environmentVariable(stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.gradleProperty(stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.systemProperty(stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    /**
     * Updates a string provider.
     * <p>
     * Update property or otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param strValue that should be lazy-resolved to a string .
     */
    @Override
    void updateStringProperty(Provider<String> provider, Object str) {
        this.updateStringProperty.accept(provider, str)
    }

    @Override
    protected ArchiveOperationsProxy getArchiveOperations() {
        this.archives
    }

    @Override
    protected ExecOperationsProxy getExecOperations() {
        this.execs
    }

    @Override
    protected GradleSysEnvProvider getPropertyProvider() {
        this.propertyProvider
    }

    private final BiConsumer<Provider<File>, Object> updateFileProperty
    private final BiConsumer<Provider<String>, Object> updateStringProperty
    private final Function<Object, ConfigurableFileTree> fileTreeFactory
    private final File projectCacheDir
    private final ProviderFactory providerFactory
    private final ProjectLayout projectLayout
    private final Provider<File> buildDir
    private final Provider<String> projectVersion
    private final Provider<String> projectGroup
    private final ArchiveOperationsProxy archives
    private final ExecOperationsProxy execs
    private final FileOperations fileOperations
    private final GradleSysEnvProvider propertyProvider
}
