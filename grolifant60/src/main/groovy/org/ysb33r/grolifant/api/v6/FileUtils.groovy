/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v6

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.file.FileCollection
import org.ysb33r.grolifant.api.core.ClassLocation
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.errors.ClassLocationException
import org.ysb33r.grolifant.internal.v4.ClassLocationImpl

import java.util.regex.Pattern

import static org.ysb33r.grolifant.api.v4.FileUtils.resolveClassLocation

/**
 * File utilities that require Gradle 6.0+
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1.0
 */
@CompileStatic
@Slf4j
class FileUtils {
    /** Returns the classpath location for a specific class.
     *
     * Works around configuration cache-related intrumentation issue in Gradle 6.5+.
     * See {@link https://github.com/gradle/gradle/issues/14727} for details.
     *
     * If the JAR name is the same as the {@code instrumentJarName}, then search the substitution collection for the
     * first hit that matches the provided pattern.
     *
     * @param aClass Class to find.
     * @param substitutionSearch Files to search. A typically example would be to look in
     * {@code rootProject.buildscript.configurations.getByName( 'classpath' )}.
     * @param substitutionMatch The pattern to look for. Typically the name of a jar with a version.
     * @param redoIfMatch A pattern that will cause a recheck if the path matches.
     *  By default this is if the filename ends in {@code .jar}.
     * @param ignoreFromPaths A pattern for which candidate files will be ignore.
     *  By default this will be files where the path is {@code caches/jar-}.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     *   on classpath
     * @throw ClassNotFoundException
     */
    static ClassLocation resolveClassLocation(
        Class aClass,
        FileCollection substitutionSearch,
        Pattern substitutionMatch,
        Pattern redoIfMatch = IS_A_JAR,
        Pattern ignoreFromPaths = GENERATED_JAR_SUBPATH
    ) {
        def entryPointClasspath = resolveClassLocation(aClass)
        if (entryPointClasspath.file) {
            log.debug "Resolved ${aClass.canonicalName} to ${entryPointClasspath.file}"
            final String canonicalPath = entryPointClasspath.file.canonicalPath
            if (LegacyLevel.PRE_6_5 && canonicalPath =~ redoIfMatch) {
                Set<File> candidates = substitutionSearch.files.findAll { File it ->
                    it.name =~ substitutionMatch
                }
                log.debug "Found ${candidates.size()} potential candidate(s): ${candidates}"
                Set<File> replacements = candidates.findAll {
                    !(it.canonicalPath =~ ignoreFromPaths)
                }
                if (replacements.empty) {
                    throw new ClassLocationException("Cannot find suitable jar replacement for ${aClass.canonicalName}")
                }
                log.debug "Selected ${replacements[0]} from ${replacements}"
                new ClassLocationImpl(replacements[0])
            }
        } else {
            entryPointClasspath
        }
    }

    private static final Pattern IS_A_JAR = ~/^.+\.jar$/
    private static final Pattern GENERATED_JAR_SUBPATH = ~/^.+caches.jar-.+$/
}
