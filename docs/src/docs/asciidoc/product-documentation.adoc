= Grolifant : A Library to Support Gradle Plugin Development
:doctype: book
include::attributes.adoc[]

.Support this project
****
If this project is useful to you, you may want to consider donating to the associated https://www.patreon.com/bePatron?u=55368021[Patreon account].
****

include::{parts}/compatibility.adoc[]

include::{parts}/bootstrap.adoc[]

= Utilities for Common Types

include::{parts}/stringutils.adoc[]

include::{parts}/fileutils.adoc[]

include::{parts}/uriutils.adoc[]

= Downloading Tools & Packages

include::{parts}/distribution-installer.adoc[]

include::{parts}/exclusive-file-access.adoc[]

= Utilities for Gradle Entities

include::{parts}/configuration-utilities.adoc[]

include::{parts}/taskutilities.adoc[]

include::{parts}/javaforkoptions.adoc[]

include::{parts}/extending-dsl.adoc[]

include::{parts}/repositories.adoc[]

include::{parts}/properties.adoc[]

= Working with Executables

include::{parts}/script-wrappers.adoc[]

include::{parts}/execspec.adoc[]

= Miscellaneous

include::{parts}/project-operations.adoc[]

include::{parts}/operatingsystem.adoc[]

include::{parts}/git-archive.adoc[]

include::{parts}/property-resolving.adoc[]

include::{parts}/grolifant.adoc[]
