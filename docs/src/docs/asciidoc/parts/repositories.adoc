== Repositories

As from Grolifant 0.10 implementation of new repository types that optionally also need to support credentials is available. Two classes are currently available:

* link:{groovydocv4}++/repositories/AuthenticationSupportedRepository.html++[AuthenticationSupportedRepository] is a base class for plugin authors to extend.
* link:{groovydocv4}++/repositories/SimplePasswordCredentials.html++[SimplePasswordCredentials] is exactly what the name says - a simple memory-based object containing a username and a password.

See also: <<ExtendingRepositoryHandler,Extending RepositoryHandler>>.
